Imports System.Text.RegularExpressions
Imports System.Text
Imports System.IO

Public Class Form1

    Public Version As String = "1.0.0a"
    Public DatabaseConn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("dbConn"))
    Public SqlQuery As SqlCommand
    Public Proxy As Boolean = False
    Public myProxy As System.Net.WebProxy
    Public sysTimer As System.Timers.Timer
    Public minTimer As System.Timers.Timer
    Public Current As Hashtable
    Public _StartXY As Point
    Public _MouseDown As Boolean
    Public Paused As Boolean = False
    Public DbConnected As Boolean = False
    Public DbErrors As Integer = 0
    Public EmptyFiles As Hashtable
    Public FirstRun As Boolean = True

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim Multiple As Boolean = False
        Dim Update As Boolean = False

        ContextMenuStrip1.ImageList = ImageList2
        ToolStripMenuItem1.ImageIndex = 3
        ViewToolStripMenuItem.ImageIndex = 7
        PauseToolStripMenuItem.ImageIndex = 4
        RunToolStripMenuItem.ImageIndex = 6

        Multiple = CheckForExistingInstance()
        Update = CheckForUpdate()
        CheckForProxy()

        If Multiple = True Then
            MessageBox.Show("Another Instance of this process is already running", "Multiple Instances Forbidden", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Application.Exit()
        End If

        If Multiple = False Then
            Xceed.Zip.Licenser.LicenseKey = "ZIN23-X0UZT-5AHMP-4A2A"
            If Update = False Then
                Call StartUp()
            End If
        Else

        End If

    End Sub

    Private Sub StartUp()

        sysTimer = New System.Timers.Timer(15000)
        AddHandler sysTimer.Elapsed, AddressOf sysOnTimedEvent
        sysTimer.AutoReset = True
        sysTimer.Enabled = True

        EmptyFiles = New Hashtable

    End Sub

    Private Sub Run()

        'DatabaseConn.ConnectionString = ""

        If Paused = False Then
            sysTimer.Enabled = False
        End If
        updateButton("", False)
        updateListbox("Starting run", True)

        GetFiles()

        If DateTime.Now.Minute = 10 Then
            GetEmptyFiles()
        End If

        Try
            DatabaseConn = New SqlConnection
            DatabaseConn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings("dbConn")
            DatabaseConn.Open()
            DbConnected = True
        Catch Ex As Exception
            Call ErrorHandler("", Ex.Message, "Error connecting to database")
            DbConnected = False
        End Try

        If DbConnected = True Then
            AddHandler DatabaseConn.StateChange, AddressOf databaseOnStateChanged

            If DbConnected = True Then
                Fixtures()
            End If
            If DbConnected = True Then
                FixturesTT()
            End If
            If DbConnected = True Then
                Matches()
            End If
            If DbConnected = True Then
                ResultsTT()
            End If
            If DbConnected = True Then
                Logs()
            End If
            If DbConnected = True Then
                LogsTT()
            End If
            If DbConnected = True Then
                News()
            End If
            If DbConnected = True Then
                Live()
            End If

            If DbConnected = True Then
                'Competitions()
            End If

            If DateTime.Now.Hour = 0 And DateTime.Now.Minute < 5 Then
                ZipFiles()
            ElseIf FirstRun = True Then
                ZipFiles()
            End If

            RemoveHandler DatabaseConn.StateChange, AddressOf databaseOnStateChanged
        End If

        Try
            DatabaseConn.Close()
        Catch

        Finally
            DatabaseConn.Dispose()
            DbConnected = False
            DbErrors = 0
        End Try

        updateListbox("Run Complete (" & DateTime.Now.ToShortTimeString & ")", True)
        updateListbox("select", True)
        updateButton("", True)
        FirstRun = False
        If Paused = False Then
            sysTimer.Enabled = True
        End If

    End Sub

    Private Sub sysOnTimedEvent(ByVal source As Object, ByVal e As ElapsedEventArgs)

        If Me.WindowState = FormWindowState.Normal Then
            minTimer = New System.Timers.Timer(300000)
            AddHandler minTimer.Elapsed, AddressOf minOnTimedEvent
            minTimer.AutoReset = False
            minTimer.Enabled = True
        Else
            minTimer = New System.Timers.Timer(300000)
            AddHandler minTimer.Elapsed, AddressOf minOnTimedEvent
            minTimer.AutoReset = False
            minTimer.Enabled = True
        End If

        updateListbox("", False)
        Dim t As Thread = New Thread(AddressOf Run)
        t.IsBackground = True
        t.Start()

    End Sub

    Private Sub minOnTimedEvent(ByVal source As Object, ByVal e As ElapsedEventArgs)

        If Me.WindowState = FormWindowState.Normal Then
            'Me.Hide()
            'Me.WindowState = FormWindowState.Minimized
            'ViewToolStripMenuItem.Text = "View"
        End If

    End Sub

    Private Sub databaseOnStateChanged(ByVal source As Object, ByVal e As System.Data.StateChangeEventArgs)

        If DbConnected = True Then
            If e.CurrentState = ConnectionState.Broken Or e.CurrentState = ConnectionState.Closed Then
                Try
                    If DatabaseConn.State = ConnectionState.Broken Then
                        DatabaseConn.Close()
                        DatabaseConn.Dispose()
                        DatabaseConn = New SqlConnection
                        DatabaseConn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings("dbConn")
                        DatabaseConn.Open()
                    ElseIf DatabaseConn.State = ConnectionState.Closed Then
                        DatabaseConn.Dispose()
                        DatabaseConn = New SqlConnection
                        DatabaseConn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings("dbConn")
                        DatabaseConn.Open()
                    End If
                Catch Ex As Exception
                    DbConnected = False
                    Call ErrorHandler("Database Error", Ex.Message, "Error connecting to database")
                End Try
            End If
        End If

    End Sub

#Region " Painting "

    Private Sub Form1_Paint(ByVal sender As Object, ByVal e As PaintEventArgs) Handles MyBase.Paint

        Dim myGraphics As Graphics = e.Graphics

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(206, 223, 246)
        Dim tmpPen As Pen = New Pen(tmpColor)

        myGraphics.DrawRectangle(tmpPen, New Rectangle(2, 2, 238, 212))

        myGraphics.DrawLine(tmpPen, 0, 3, 0, 213)
        myGraphics.DrawLine(tmpPen, 1, 1, 1, 214)

        myGraphics.DrawLine(tmpPen, 3, 0, 239, 0)
        myGraphics.DrawLine(tmpPen, 1, 1, 241, 1)

        myGraphics.DrawLine(tmpPen, 241, 1, 241, 214)
        myGraphics.DrawLine(tmpPen, 242, 3, 242, 213)

        myGraphics.DrawLine(tmpPen, 1, 215, 241, 215)
        myGraphics.DrawLine(tmpPen, 3, 216, 239, 216)

        myGraphics.DrawLine(tmpPen, 1, 1, 2, 2)
        myGraphics.DrawLine(tmpPen, 2, 1, 2, 2)

        myGraphics.DrawLine(tmpPen, 240, 2, 241, 1)
        myGraphics.DrawLine(tmpPen, 240, 1, 240, 2)

        myGraphics.DrawLine(tmpPen, 1, 215, 2, 214)
        myGraphics.DrawLine(tmpPen, 2, 214, 2, 215)

        myGraphics.DrawLine(tmpPen, 240, 215, 241, 215)
        myGraphics.DrawLine(tmpPen, 240, 214, 240, 215)

    End Sub

    Private Sub Panel1_Paint(ByVal sender As Object, ByVal e As PaintEventArgs) Handles Panel1.Paint

        Dim myGraphics As Graphics = e.Graphics

        Dim myRectangle As Rectangle = New Rectangle(0, 0, 237, 130)
        myGraphics.DrawRectangle(Pens.Transparent, myRectangle)
        Dim StartColor As System.Drawing.Color = System.Drawing.Color.FromArgb(68, 118, 185)
        Dim EndColor As System.Drawing.Color = System.Drawing.Color.FromArgb(206, 223, 246)
        Dim tmpPen As Pen = New System.Drawing.Pen(EndColor)
        Dim myBrush As System.Drawing.Drawing2D.LinearGradientBrush
        myBrush = New System.Drawing.Drawing2D.LinearGradientBrush(myRectangle, StartColor, EndColor, System.Drawing.Drawing2D.LinearGradientMode.Vertical)
        myGraphics.FillRectangle(myBrush, myRectangle)

    End Sub

    Private Sub Panel2_Paint(ByVal sender As Object, ByVal e As PaintEventArgs) Handles Panel2.Paint

        Dim myGraphics As Graphics = e.Graphics
        Dim Color1 As Color = System.Drawing.Color.FromArgb(18, 52, 113)

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(206, 223, 246)
        Dim tmpPen As Pen = New Pen(tmpColor)

        myGraphics.DrawLine(tmpPen, 0, 0, 1, 0)
        myGraphics.DrawLine(tmpPen, 0, 0, 0, 1)

        myGraphics.DrawLine(tmpPen, 235, 0, 236, 0)
        myGraphics.DrawLine(tmpPen, 236, 0, 236, 1)

        Dim tmpIcon As System.Drawing.Icon = New System.Drawing.Icon("C:\Program Files\SuperSport Zone\SuperSport Rugby\appxp.ico")
        myGraphics.DrawIcon(tmpIcon, 5, 5)

    End Sub

#End Region

#Region " Mouse Events "

    Private Sub pnl1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel2.MouseDown

        Me._StartXY = Me.MousePosition
        Me._MouseDown = True

    End Sub

    Private Sub pnl1_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel2.MouseMove

        If Me._MouseDown Then
            Dim x As Integer, y As Integer
            Dim Xdiff As Integer, Ydiff As Integer
            Xdiff = Me.MousePosition.X - Me._StartXY.X
            Ydiff = Me.MousePosition.Y - Me._StartXY.Y
            Me.Left = Me.Left + Xdiff
            Me.Top = Me.Top + Ydiff
            Me._StartXY = Me.MousePosition
        End If

    End Sub

    Private Sub pnl1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel2.MouseUp

        MyBase.OnMouseUp(e)
        Me._MouseDown = False

    End Sub

    Private Sub Button1_MouseOver(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Button1.MouseMove

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(45, 115, 212)
        Button1.FlatAppearance.BorderColor = tmpColor

    End Sub

    Private Sub Button1_MouseOut(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.MouseLeave

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(206, 223, 246)
        Button1.FlatAppearance.BorderColor = tmpColor

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        ListBox1.Focus()
        ListBox1.Items.Clear()
        Dim t As Thread = New Thread(AddressOf Run)
        t.IsBackground = True
        t.Start()

    End Sub

    Private Sub Button2_MouseOver(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Button2.MouseMove

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(45, 115, 212)
        Button2.FlatAppearance.BorderColor = tmpColor

    End Sub

    Private Sub Button2_MouseOut(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.MouseLeave

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(206, 223, 246)
        Button2.FlatAppearance.BorderColor = tmpColor

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        ListBox1.Focus()
        If Button2.ImageIndex = 1 Then
            Button2.ImageIndex = 2
            sysTimer.Enabled = False
            Paused = True
            updateListbox("", False)
            updateListbox("Application paused", True)
            PauseToolStripMenuItem.Text = "Start"
            PauseToolStripMenuItem.ImageIndex = 5
        Else
            Button2.ImageIndex = 1
            sysTimer.Enabled = True
            Paused = False
            updateListbox("", False)
            updateListbox("Application started", True)
            PauseToolStripMenuItem.Text = "Pause"
            PauseToolStripMenuItem.ImageIndex = 4
        End If

    End Sub

    Private Sub Button3_MouseOver(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Button3.MouseMove

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(206, 223, 246)
        Button3.FlatAppearance.BorderColor = tmpColor

    End Sub

    Private Sub Button3_MouseOut(ByVal sender As Object, ByVal e As EventArgs) Handles Button3.MouseLeave

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(68, 118, 185)
        Button3.FlatAppearance.BorderColor = tmpColor

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        Me.Close()
        Me.Dispose()

    End Sub

    Private Sub Button4_MouseOver(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Button4.MouseMove

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(206, 223, 246)
        Button4.FlatAppearance.BorderColor = tmpColor

    End Sub

    Private Sub Button4_MouseOut(ByVal sender As Object, ByVal e As EventArgs) Handles Button4.MouseLeave

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(68, 118, 185)
        Button4.FlatAppearance.BorderColor = tmpColor

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click

        ListBox1.Focus()
        ViewToolStripMenuItem.Text = "View"
        ViewToolStripMenuItem.ImageIndex = 8
        Me.Hide()
        Me.WindowState = FormWindowState.Minimized

    End Sub

    Private Sub ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem1.Click

        Me.Close()
        Me.Dispose()

    End Sub

    Private Sub ViewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewToolStripMenuItem.Click

        If ViewToolStripMenuItem.Text = "View" Then
            Me.WindowState = FormWindowState.Normal
            Me.Show()
            ViewToolStripMenuItem.Text = "Hide"
            ViewToolStripMenuItem.ImageIndex = 7
        Else
            Me.Hide()
            Me.WindowState = FormWindowState.Minimized
            ViewToolStripMenuItem.Text = "View"
            ViewToolStripMenuItem.ImageIndex = 8
        End If

    End Sub

    Private Sub RunToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RunToolStripMenuItem.Click

        ListBox1.Focus()
        ListBox1.Items.Clear()
        Dim t As Thread = New Thread(AddressOf Run)
        t.IsBackground = True
        t.Start()

    End Sub

    Private Sub PauseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PauseToolStripMenuItem.Click

        ListBox1.Focus()
        If Button2.ImageIndex = 1 Then
            Button2.ImageIndex = 2
            sysTimer.Enabled = False
            Paused = True
            updateListbox("", False)
            updateListbox("Application paused", True)
            PauseToolStripMenuItem.Text = "Start"
            PauseToolStripMenuItem.ImageIndex = 5
        Else
            Button2.ImageIndex = 1
            sysTimer.Enabled = True
            Paused = False
            updateListbox("", False)
            updateListbox("Application started", True)
            PauseToolStripMenuItem.Text = "Pause"
            PauseToolStripMenuItem.ImageIndex = 4
        End If

    End Sub

    Private Sub NotifyIcon1_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick

        If Me.WindowState = FormWindowState.Normal Then
            Me.Activate()
            ViewToolStripMenuItem.Text = "Hide"
        Else
            Me.WindowState = FormWindowState.Normal
            Me.Show()
            ViewToolStripMenuItem.Text = "Hide"
        End If

    End Sub

#End Region

#Region " GetFiles "

    Private Sub GetFiles()

        'Try
        '    Dim File As String
        '    Dim Files() As String
        '    Dim Fi As FileInfo

        '    If Directory.Exists("C:\inetpub\ftproot\pauser\rugby") Then
        '        Files = Directory.GetFiles("C:\inetpub\ftproot\pauser\rugby")

        '        For Each File In Files
        '            Fi = New FileInfo(File)
        '            If Fi.Name.IndexOf(".xml") >= 0 And Fi.Length > 0 Then
        '                Dim Filename As String = String.Empty
        '                If Fi.Name.StartsWith("rbu_fix_") Then
        '                    Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture\Temporary\" & Fi.Name & ""
        '                ElseIf Fi.Name.StartsWith("rbu_res") Then
        '                    Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Match\Temporary\" & Fi.Name & ""
        '                ElseIf Fi.Name.StartsWith("rbu_ltl") Then
        '                    Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log\Temporary\" & Fi.Name & ""
        '                ElseIf Fi.Name.StartsWith("rbu_lts") Then
        '                    Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log\Complete\" & Fi.Name & ""
        '                ElseIf Fi.Name.IndexOf("LiveRugby") >= 0 Then
        '                    Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Live\Temporary\" & Fi.Name & ""
        '                ElseIf Fi.Name.IndexOf("RugbyCommentary") >= 0 Then
        '                    Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Live\Temporary\" & Fi.Name & ""
        '                ElseIf Fi.Name.IndexOf("Schedules_") >= 0 Then
        '                    Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture TT\Temporary\" & Fi.Name & ""
        '                ElseIf Fi.Name.IndexOf("Results_") >= 0 Then
        '                    Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Result TT\Temporary\" & Fi.Name & ""
        '                ElseIf Fi.Name.IndexOf("Tables_") >= 0 Then
        '                    Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log TT\Temporary\" & Fi.Name & ""
        '                Else
        '                    Dim tmpValue As String = Fi.Name.Substring(0, 1)
        '                    If IsNumeric(tmpValue) Then
        '                        Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\News\Temporary\" & Fi.Name & ""
        '                    Else
        '                        Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\" & Fi.Name & ""
        '                    End If
        '                End If

        '                If Not String.IsNullOrEmpty(Filename) Then
        '                    Fi.CopyTo(Filename, True)
        '                    Fi.Delete()
        '                End If
        '            End If
        '        Next
        '    End If
        'Catch ex As Exception
        '    Call ErrorHandler("", ex.Message, "Error getting files from internal PA ftp folder")
        'End Try

        Try
            Dim File As String
            Dim Files() As String
            Dim Fi As FileInfo

            If Directory.Exists("C:\inetpub\ftproot\teamtalk\rugby") Then
                Files = Directory.GetFiles("C:\inetpub\ftproot\teamtalk\rugby")

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And Fi.Length > 0 Then
                        Dim Filename As String = String.Empty
                        If Fi.Name.StartsWith("rbu_fix_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("rbu_res") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Match\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("rbu_ltl") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("rbu_lts") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log\Complete\" & Fi.Name & ""
                        ElseIf Fi.Name.IndexOf("LiveRugby") >= 0 Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Live\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.IndexOf("Live_Rugby") >= 0 Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Live\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.IndexOf("RugbyCommentary") >= 0 Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Live\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.IndexOf("Schedules_") >= 0 Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture TT\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.IndexOf("Results_") >= 0 Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Result TT\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.IndexOf("Tables_") >= 0 Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log TT\Temporary\" & Fi.Name & ""
                        Else
                            Dim tmpValue As String = Fi.Name.Substring(0, 1)
                            If IsNumeric(tmpValue) Then
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\News\Temporary\" & Fi.Name & ""
                            Else
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\" & Fi.Name & ""
                            End If
                        End If

                        If Not String.IsNullOrEmpty(Filename) Then
                            Fi.CopyTo(Filename, True)
                            Fi.Delete()
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            Call ErrorHandler("", ex.Message, "Error getting files from internal Team Talk ftp folder")
        End Try

        'Dim tmpFtp As Rebex.Net.Ftp = New Rebex.Net.Ftp
        'Dim Server As String = "152.111.116.3"
        'Dim UserName As String = "PASport"
        'Dim Password As String = "Supersp0rt"
        'Dim DirectoryName As String = "rugby"
        'Dim DisconnectCount As Integer = 0

        'Try
        '    updateListbox("Checking for new data", True)
        '    tmpFtp.Connect(Server)
        '    tmpFtp.Login(UserName, Password)
        '    tmpFtp.SetTransferType(Rebex.Net.FtpTransferType.Binary)
        '    tmpFtp.KeepAlive()
        '    If DirectoryName <> "" Then
        '        tmpFtp.ChangeDirectory("rugby")
        '    End If
        '    Dim List As Rebex.Net.FtpList
        '    List = tmpFtp.GetList()
        '    Dim I As Integer
        '    Dim Total As Integer = 0
        '    If List.Count > 0 Then
        '        For I = 0 To List.Count - 1
        '            Dim Item As Rebex.Net.FtpItem = List(I)
        '            If Item.Name.IndexOf(".xml") >= 0 Then
        '                Total = Total + 1
        '            End If
        '        Next
        '    End If
        '    If Total > 0 Then
        '        Dim FilesDownloaded As Integer = 0
        '        Dim FileErrors As Integer = 0
        '        updateListbox("Downloading files", True)
        '        updateListbox(Total & " files remaining", True)
        '        For I = 0 To List.Count - 1
        '            Dim Download As Boolean = False
        '            Dim Item As Rebex.Net.FtpItem = List(I)
        '            Dim Filename As String = ""
        '            If Item.Name.IndexOf(".xml") >= 0 Then
        '                If Item.Size > 0 Then
        '                    Download = True
        '                    If Item.Name.StartsWith("rbu_fix_") Then
        '                        Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture\Temporary\" & Item.Name & ""
        '                    ElseIf Item.Name.StartsWith("rbu_res") Then
        '                        Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Match\Temporary\" & Item.Name & ""
        '                    ElseIf Item.Name.StartsWith("rbu_ltl") Then
        '                        Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log\Temporary\" & Item.Name & ""
        '                    ElseIf Item.Name.StartsWith("rbu_lts") Then
        '                        Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log\Complete\" & Item.Name & ""
        '                    ElseIf Item.Name.IndexOf("LiveRugby") >= 0 Then
        '                        Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Live\Temporary\" & Item.Name & ""
        '                    ElseIf Item.Name.IndexOf("RugbyCommentary") >= 0 Then
        '                        Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Live\Temporary\" & Item.Name & ""
        '                    ElseIf Item.Name.IndexOf("Schedules_") >= 0 Then
        '                        Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture TT\Temporary\" & Item.Name & ""
        '                    ElseIf Item.Name.IndexOf("Results_") >= 0 Then
        '                        Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Result TT\Temporary\" & Item.Name & ""
        '                    ElseIf Item.Name.IndexOf("Tables_") >= 0 Then
        '                        Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log TT\Temporary\" & Item.Name & ""
        '                    Else
        '                        Dim tmpValue As String = Item.Name.Substring(0, 1)
        '                        If IsNumeric(tmpValue) Then
        '                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\News\Temporary\" & Item.Name & ""
        '                        Else
        '                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\" & Item.Name & ""
        '                        End If
        '                    End If
        '                Else
        '                    If EmptyFiles.ContainsKey(Filename) Then
        '                        If EmptyFiles(Filename) > 1 Then
        '                            Download = True
        '                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\empty_" & Item.Name & ""
        '                            EmptyFiles.Remove(Filename)
        '                        Else
        '                            EmptyFiles.Remove(Filename)
        '                            EmptyFiles.Add(Filename, 2)
        '                        End If
        '                    Else
        '                        EmptyFiles.Add(Filename, 1)
        '                    End If
        '                End If
        '            End If

        '            If Download = True Then
        '                Try
        '                    tmpFtp.GetFile(Item.Name, Filename)
        '                    tmpFtp.KeepAlive()
        '                    tmpFtp.DeleteFile(Item.Name)
        '                    tmpFtp.KeepAlive()
        '                    FilesDownloaded = FilesDownloaded + 1
        '                Catch ex1 As Exception
        '                    If File.Exists(Filename) Then
        '                        Try
        '                            File.Delete(Filename)
        '                        Catch ex2 As Exception
        '                            Call ErrorHandler(Item.Name, ex2.Message, "Error deleting file")
        '                        End Try
        '                    End If
        '                    FileErrors = FileErrors + 1
        '                    If tmpFtp.State = Rebex.Net.FtpState.Disconnected Then
        '                        DisconnectCount = DisconnectCount + 1
        '                        If DisconnectCount = 5 Then
        '                            Call ErrorHandler(Item.Name, ex1.Message, "To many disconnects")
        '                            Exit For
        '                        Else
        '                            Try
        '                                tmpFtp.Connect(Server)
        '                                tmpFtp.Login(UserName, Password)
        '                                tmpFtp.SetTransferType(Rebex.Net.FtpTransferType.Binary)
        '                                tmpFtp.KeepAlive()
        '                            Catch ex2 As Exception
        '                                Call ErrorHandler(Item.Name, ex2.Message, "Unable to connect")
        '                                Exit For
        '                            End Try
        '                        End If
        '                    Else
        '                        Call ErrorHandler(Item.Name, ex1.Message, "Error downloading the file")
        '                    End If
        '                End Try

        '                updateListbox(Total - (FilesDownloaded + FileErrors) + 1 & " files remaining", False)
        '                If FilesDownloaded + FileErrors = Total Then
        '                    updateListbox(FilesDownloaded & " files downloaded with " & FileErrors & " errors", True)
        '                Else
        '                    updateListbox(Total - (FilesDownloaded + FileErrors) & " files remaining", True)
        '                End If
        '            End If
        '        Next
        '    Else
        '        updateListbox("No new files", True)
        '    End If
        '    tmpFtp.Disconnect()
        'Catch ex As Exception
        '    Call ErrorHandler("", ex.Message, "Error with ftp")
        'Finally
        '    tmpFtp.Dispose()
        'End Try

    End Sub

    Private Sub GetEmptyFiles()

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files") Then
            Dim File As String
            Dim Files() As String
            Dim Fi As FileInfo
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files")
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf("empty") >= 0 Then
                    If Fi.Length = 0 Then
                        Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Empty/" & Fi.Name & "", True)
                        Fi.Delete()
                    Else
                        If Fi.Name.StartsWith("empty_m") Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Match\Temporary/" & Fi.Name.Replace("empty_", "") & "", True)
                            Fi.Delete()
                        ElseIf Fi.Name.StartsWith("empty_f") Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture\Temporary/" & Fi.Name.Replace("empty_", "") & "", True)
                            Fi.Delete()
                        End If
                    End If
                End If
            Next
        End If

    End Sub

#End Region

#Region " Competitions "

    Private Sub Competitions()

        Dim File As String
        Dim Files() As String
        Dim Fi As FileInfo

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Competition\Temporary") Then
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Competition\Temporary")
            Dim Count As Integer = 0
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf(".xml") >= 0 And Fi.Name.IndexOf("rbu_cmp") >= 0 Then
                    Count = Count + 1
                End If
            Next
            If Count > 0 Then
                updateListbox("Processing competitions", True)
                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And Fi.Name.IndexOf("rbu_cmp") >= 0 Then
                        Dim MoveFile As Boolean = True
                        Try
                            Dim xmlDoc As New XmlDocument
                            xmlDoc.Load(File)

                            Dim xmlCompetitions As XmlNodeList = xmlDoc.SelectNodes("Competitions/Competition")
                            Dim I As Integer
                            For I = 0 To xmlCompetitions.Count - 1
                                Dim Competition_Id As Integer
                                Dim Competition_Sponsor As String = ""
                                Dim Competition_Name As String = ""
                                Dim Competition_StartDate As DateTime = New DateTime(1900, 1, 1)
                                Dim Competition_EndDate As DateTime = New DateTime(1900, 1, 1)

                                Dim tmpNodes As XmlAttributeCollection = xmlCompetitions(I).Attributes
                                Dim M As Integer
                                For M = 0 To tmpNodes.Count - 1
                                    Dim tmpName As String = tmpNodes(M).Name
                                    Select Case tmpName
                                        Case "id"
                                            Competition_Id = tmpNodes(M).InnerText
                                        Case "sponsor"
                                            Competition_Sponsor = tmpNodes(M).InnerText
                                        Case "name"
                                            Competition_Name = tmpNodes(M).InnerText
                                        Case "startDate"
                                            Dim tmpDate As String = tmpNodes(M).InnerText
                                            Dim tmpYear As String = tmpDate.Substring(0, 4)
                                            Dim tmpMonth As String = tmpDate.Substring(4, 2)
                                            Dim tmpDay As String = tmpDate.Substring(6, 2)
                                            If tmpMonth.StartsWith("0") Then
                                                tmpMonth = tmpMonth.Substring(1, 1)
                                            End If
                                            If tmpDay.StartsWith("0") Then
                                                tmpDay = tmpDay.Substring(1, 1)
                                            End If
                                            Competition_StartDate = New DateTime(tmpYear, tmpMonth, tmpDay, 0, 0, 0)
                                        Case "endDate"
                                            Dim tmpDate As String = tmpNodes(M).InnerText
                                            Dim tmpYear As String = tmpDate.Substring(0, 4)
                                            Dim tmpMonth As String = tmpDate.Substring(4, 2)
                                            Dim tmpDay As String = tmpDate.Substring(6, 2)
                                            If tmpMonth.StartsWith("0") Then
                                                tmpMonth = tmpMonth.Substring(1, 1)
                                            End If
                                            If tmpDay.StartsWith("0") Then
                                                tmpDay = tmpDay.Substring(1, 1)
                                            End If
                                            Competition_EndDate = New DateTime(tmpYear, tmpMonth, tmpDay, 0, 0, 0)
                                        Case Else

                                    End Select
                                Next

                                SqlQuery = New SqlCommand("Delete From Rugby.dbo.pa_Competitions Where Competition_Id = " & Competition_Id & " And (Competition_Season = 10)", DatabaseConn)
                                SqlQuery.ExecuteNonQuery()

                                Dim xmlStages As XmlNodeList = xmlCompetitions(I).SelectNodes("Stages/Stage")
                                For M = 0 To xmlStages.Count - 1
                                    Dim Stage_Number As Integer
                                    Dim Stage_Name As String = ""
                                    Dim Stage_StartDate As DateTime = New DateTime(1900, 1, 1)
                                    Dim Stage_EndDate As DateTime = New DateTime(1900, 1, 1)
                                    Dim Stage_Type As String

                                    tmpNodes = xmlStages(M).Attributes
                                    Dim N As Integer
                                    For N = 0 To tmpNodes.Count - 1
                                        Dim tmpName As String = tmpNodes(N).Name
                                        Select Case tmpName
                                            Case "number"
                                                Stage_Number = tmpNodes(N).InnerText
                                            Case "type"
                                                Stage_Type = tmpNodes(N).InnerText
                                            Case "name"
                                                Stage_Name = tmpNodes(N).InnerText
                                            Case "startDate"
                                                Dim tmpDate As String = tmpNodes(N).InnerText
                                                Dim tmpYear As String = tmpDate.Substring(0, 4)
                                                Dim tmpMonth As String = tmpDate.Substring(4, 2)
                                                Dim tmpDay As String = tmpDate.Substring(6, 2)
                                                If tmpMonth.StartsWith("0") Then
                                                    tmpMonth = tmpMonth.Substring(1, 1)
                                                End If
                                                If tmpDay.StartsWith("0") Then
                                                    tmpDay = tmpDay.Substring(1, 1)
                                                End If
                                                Stage_StartDate = New DateTime(tmpYear, tmpMonth, tmpDay, 0, 0, 0)
                                            Case "endDate"
                                                Dim tmpDate As String = tmpNodes(N).InnerText
                                                Dim tmpYear As String = tmpDate.Substring(0, 4)
                                                Dim tmpMonth As String = tmpDate.Substring(4, 2)
                                                Dim tmpDay As String = tmpDate.Substring(6, 2)
                                                If tmpMonth.StartsWith("0") Then
                                                    tmpMonth = tmpMonth.Substring(1, 1)
                                                End If
                                                If tmpDay.StartsWith("0") Then
                                                    tmpDay = tmpDay.Substring(1, 1)
                                                End If
                                                Stage_EndDate = New DateTime(tmpYear, tmpMonth, tmpDay, 0, 0, 0)
                                            Case Else

                                        End Select
                                    Next

                                    Dim xmlRounds As XmlNodeList = xmlStages(M).SelectNodes("Rounds/Round")
                                    If xmlRounds.Count > 0 Then
                                        For N = 0 To xmlRounds.Count - 1
                                            Dim Round_Number As Integer
                                            Dim Round_Name As String = ""
                                            Dim Round_StartDate As DateTime = New DateTime(1900, 1, 1)
                                            Dim Round_EndDate As DateTime = New DateTime(1900, 1, 1)
                                            Dim Round_Legs As Integer = 1
                                            Dim Round_Replay As Integer = 0

                                            tmpNodes = xmlRounds(N).Attributes
                                            Dim O As Integer
                                            For O = 0 To tmpNodes.Count - 1
                                                Dim tmpName As String = tmpNodes(O).Name
                                                Select Case tmpName
                                                    Case "number"
                                                        Round_Number = tmpNodes(O).InnerText
                                                    Case "name"
                                                        Round_Name = tmpNodes(O).InnerText
                                                    Case "startDate"
                                                        Dim tmpDate As String = tmpNodes(O).InnerText
                                                        Dim tmpYear As String = tmpDate.Substring(0, 4)
                                                        Dim tmpMonth As String = tmpDate.Substring(4, 2)
                                                        Dim tmpDay As String = tmpDate.Substring(6, 2)
                                                        If tmpMonth.StartsWith("0") Then
                                                            tmpMonth = tmpMonth.Substring(1, 1)
                                                        End If
                                                        If tmpDay.StartsWith("0") Then
                                                            tmpDay = tmpDay.Substring(1, 1)
                                                        End If
                                                        Round_StartDate = New DateTime(tmpYear, tmpMonth, tmpDay, 0, 0, 0)
                                                    Case "endDate"
                                                        Dim tmpDate As String = tmpNodes(O).InnerText
                                                        Dim tmpYear As String = tmpDate.Substring(0, 4)
                                                        Dim tmpMonth As String = tmpDate.Substring(4, 2)
                                                        Dim tmpDay As String = tmpDate.Substring(6, 2)
                                                        If tmpMonth.StartsWith("0") Then
                                                            tmpMonth = tmpMonth.Substring(1, 1)
                                                        End If
                                                        If tmpDay.StartsWith("0") Then
                                                            tmpDay = tmpDay.Substring(1, 1)
                                                        End If
                                                        Round_EndDate = New DateTime(tmpYear, tmpMonth, tmpDay, 0, 0, 0)
                                                    Case "legs"
                                                        Round_Legs = tmpNodes(O).InnerText
                                                    Case "replaysPossible"
                                                        If tmpNodes(O).InnerText = "Yes" Then
                                                            Round_Replay = 1
                                                        End If
                                                    Case Else

                                                End Select
                                            Next

                                            Dim Insert As Boolean = False
                                            Insert = True

                                            If Insert = True Then
                                                SqlQuery = New SqlCommand("Insert Into Rugby.dbo.pa_Competitions (Competition_Id, Competition_Season, Competition_Name, Competition_Sponsor, Competition_Startdate, Competition_Enddate, Stage_Number, Stage_Name, Stage_Startdate, Stage_Enddate, Stage_Type, Round_Number, Round_Name, Round_Startdate, Round_Enddate, Round_Legs, Round_Replay, Created) Values (" & Competition_Id & ", 10, '" & Competition_Name.Replace("'", "''") & "', '" & Competition_Sponsor.Replace("'", "''") & "', '" & Competition_StartDate & "', '" & Competition_EndDate & "', " & Stage_Number & ", '" & Stage_Name.Replace("'", "''") & "', '" & Stage_StartDate & "', '" & Stage_EndDate & "', '" & Stage_Type.Replace("'", "''") & "', " & Round_Number & ", '" & Round_Name.Replace("'", "''") & "', '" & Round_StartDate & "', '" & Round_EndDate & "', " & Round_Legs & ", " & Round_Replay & ", '" & DateTime.Now.ToString & "')", DatabaseConn)
                                                SqlQuery.ExecuteNonQuery()
                                            End If
                                        Next
                                    Else
                                        Dim Insert As Boolean = False
                                        Insert = True

                                        If Insert = True Then
                                            SqlQuery = New SqlCommand("Insert Into Rugby.dbo.pa_Competitions (Competition_Id, Competition_Season, Competition_Name, Competition_Sponsor, Competition_Startdate, Competition_Enddate, Stage_Number, Stage_Name, Stage_Startdate, Stage_Enddate, Stage_Type, Round_Number, Round_Name, Round_Startdate, Round_Enddate, Round_Legs, Round_Replay, Created) Values (" & Competition_Id & ", 10, '" & Competition_Name.Replace("'", "''") & "', '" & Competition_Sponsor.Replace("'", "''") & "', '" & Competition_StartDate & "', '" & Competition_EndDate & "', " & Stage_Number & ", '" & Stage_Name.Replace("'", "''") & "', '" & Stage_StartDate & "', '" & Stage_EndDate & "', '" & Stage_Type.Replace("'", "''") & "', 0, '', '', '', 0, 0, '" & DateTime.Now.ToString & "')", DatabaseConn)
                                            SqlQuery.ExecuteNonQuery()
                                        End If
                                    End If
                                Next
                            Next
                            xmlCompetitions = Nothing

                            xmlDoc = Nothing
                        Catch ex As Exception
                            MoveFile = False
                            Call ErrorHandler("", ex.Message, "Error with the competitions")
                        End Try
                        If MoveFile = True Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Competition\Complete\" & Fi.Name & "", True)
                        Else
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Competition\ByPassed\" & Fi.Name & "", True)
                        End If
                        Fi.Delete()
                    End If
                Next
                If Files.Length > 0 Then
                    updateListbox("Competitions complete", True)
                End If
            End If
        End If

    End Sub

#End Region

#Region " Fixtures "

    Private Sub Fixtures()

        Dim File As String
        Dim Files() As String
        Dim Fi As FileInfo
        Dim MatchId As Integer
        Dim DbCount As Integer = 0
        Dim DatabaseError As Boolean = False

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture\Temporary") Then
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture\Temporary")

            Dim Count As Integer = 0
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf(".xml") >= 0 Then
                    Count = Count + 1
                End If
            Next

            If Count > 0 Then
                updateListbox("Processing fixtures", True)

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And DbConnected = True Then
                        Dim MoveFile As Boolean = True
                        Try
                            Dim xmlDoc As New XmlDocument
                            xmlDoc.Load(File)

                            Dim xmlTeams As XmlNodeList = xmlDoc.SelectNodes("Fixtures/Teams/Team")
                            Dim I As Integer
                            Dim Teams As Hashtable = New Hashtable
                            Teams.Add("", "")
                            For I = 0 To xmlTeams.Count - 1
                                Dim tmpId As String = xmlTeams(I).Attributes.ItemOf("id").InnerText
                                Dim tmpName As String = xmlTeams(I).Attributes.ItemOf("name").InnerText
                                Teams.Add(tmpId, tmpName)
                            Next
                            xmlTeams = Nothing

                            Dim xmlMatches As XmlNodeList = xmlDoc.SelectNodes("Fixtures/Fixture")
                            For I = 0 To xmlMatches.Count - 1
                                Dim Id As String = xmlMatches(I).Attributes.ItemOf("id").InnerText
                                MatchId = Id
                                Dim Competition As String = xmlMatches(I).Attributes.ItemOf("competitionId").InnerText
                                Dim Stage As Integer = xmlMatches(I).Attributes.ItemOf("stageNumber").InnerText
                                SqlQuery = New SqlCommand("Select Top 1 Competition_Season From Rugby.dbo.pa_Competitions Where (Competition_Id = " & Competition & ") Order By Competition_Season Desc", DatabaseConn)
                                Dim Season As Integer = SqlQuery.ExecuteScalar

                                Dim Round As Integer = 0
                                If Not xmlMatches(I).Attributes.ItemOf("roundNumber") Is Nothing Then
                                    Round = xmlMatches(I).Attributes.ItemOf("roundNumber").InnerText
                                End If

                                Dim Leg As Integer = 0
                                If Not xmlMatches(I).Attributes.ItemOf("leg") Is Nothing Then
                                    Leg = xmlMatches(I).Attributes.ItemOf("leg").InnerText
                                End If

                                Dim Replay As Integer = 0
                                If Not xmlMatches(I).Attributes.ItemOf("replay") Is Nothing Then
                                    Replay = xmlMatches(I).Attributes.ItemOf("replay").InnerText
                                End If

                                Dim tmpDate As String = xmlMatches(I).Attributes.ItemOf("date").InnerText
                                Dim tmpYear As String = tmpDate.Substring(0, 4)
                                Dim tmpMonth As String = tmpDate.Substring(4, 2)
                                Dim tmpDay As String = tmpDate.Substring(6, 2)
                                If tmpMonth.StartsWith("0") Then
                                    tmpMonth = tmpMonth.Substring(1, 1)
                                End If
                                If tmpDay.StartsWith("0") Then
                                    tmpDay = tmpDay.Substring(1, 1)
                                End If
                                Dim MatchDate As DateTime = New DateTime(tmpYear, tmpMonth, tmpDay, 0, 0, 0)

                                If Not xmlMatches(I).Attributes.ItemOf("time") Is Nothing Then
                                    Dim tmpArray() As String = xmlMatches(I).Attributes.ItemOf("time").InnerText.Split("+")
                                    Dim Hour1 As String = tmpArray(0).Substring(0, 2)
                                    If Hour1.StartsWith("0") Then
                                        Hour1 = Hour1.Substring(1, 1)
                                    End If
                                    Dim Minute1 As String = tmpArray(0).Substring(2, 2)
                                    If Minute1.StartsWith("0") Then
                                        Minute1 = Minute1.Substring(1, 1)
                                    End If
                                    Dim Hour2 As String = tmpArray(1).Substring(0, 2)
                                    If Hour2.StartsWith("0") Then
                                        Hour2 = Hour2.Substring(1, 1)
                                    End If
                                    Dim Minute2 As String = tmpArray(1).Substring(2, 2)
                                    If Minute2.StartsWith("0") Then
                                        Minute2 = Minute2.Substring(1, 1)
                                    End If
                                    MatchDate = New DateTime(MatchDate.Year, MatchDate.Month, MatchDate.Day, Hour1, Minute1, 0)
                                    MatchDate = MatchDate.AddHours(0 - Hour2)
                                    MatchDate = MatchDate.AddMinutes(0 - Minute2)
                                    MatchDate = MatchDate.AddHours(2)
                                End If

                                Dim Venue As String = ""
                                If Not xmlMatches(I).Attributes.ItemOf("venue") Is Nothing Then
                                    Venue = xmlMatches(I).Attributes.ItemOf("venue").InnerText
                                End If

                                Dim Country As String = ""
                                If Not xmlMatches(I).Attributes.ItemOf("country") Is Nothing Then
                                    Country = xmlMatches(I).Attributes.ItemOf("country").InnerText
                                End If

                                Dim PoolsNo As String = ""
                                If Not xmlMatches(I).Attributes.ItemOf("poolsNo") Is Nothing Then
                                    PoolsNo = xmlMatches(I).Attributes.ItemOf("poolsNo").InnerText
                                End If

                                Dim Status As String = ""
                                Dim StatusId As Integer = 0
                                If Not xmlMatches(I).Attributes.ItemOf("status") Is Nothing Then
                                    Status = xmlMatches(I).Attributes.ItemOf("status").InnerText
                                    StatusId = retStatus(Status.ToLower)
                                End If

                                Dim xmlParticipants As XmlNodeList = xmlMatches(I).SelectNodes("Participant")
                                Dim N As Integer
                                Dim HomeTeamId As String = ""
                                Dim AwayTeamId As String = ""
                                For N = 0 To xmlParticipants.Count - 1
                                    If N = 0 Then
                                        HomeTeamId = xmlParticipants(N).Attributes.ItemOf("teamId").InnerText
                                    Else
                                        AwayTeamId = xmlParticipants(N).Attributes.ItemOf("teamId").InnerText
                                    End If
                                Next
                                Dim HomeTeamName As String = Teams(HomeTeamId)
                                Dim AwayTeamName As String = Teams(AwayTeamId)

                                SqlQuery = New SqlCommand("Execute Rugby.dbo.pa_SetTeamNames " & Competition & ", " & Season & ", " & HomeTeamId & ", '" & HomeTeamName.Replace("'", "''") & "'", DatabaseConn)
                                SqlQuery.ExecuteNonQuery()

                                SqlQuery = New SqlCommand("Execute Rugby.dbo.pa_SetTeamNames " & Competition & ", " & Season & ", " & AwayTeamId & ", '" & AwayTeamName.Replace("'", "''") & "'", DatabaseConn)
                                SqlQuery.ExecuteNonQuery()

                                Dim Present As Boolean = True
                                Dim tmpId As Integer

                                SqlQuery = New SqlCommand("Select StatusId From Rugby.dbo.pa_Matches Where (Id = " & Id & ")", DatabaseConn)
                                If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                                    Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
                                    If RsRec.HasRows() Then
                                        While RsRec.Read
                                            tmpId = RsRec("StatusId")
                                        End While
                                    Else
                                        Present = False
                                    End If
                                    RsRec.Close()
                                    If Present = True Then
                                        If StatusId >= tmpId Then
                                            SqlQuery = New SqlCommand("Update Rugby.dbo.pa_Matches Set Competition = '" & Competition.Replace("'", "''").Trim() & "', Season = " & Season & ", Stage = " & Stage & ", Round = " & Round & ", Leg = " & Leg & ", Replay = " & Replay & ", PoolsNo = '" & PoolsNo.Replace("'", "''").Trim() & "', MatchDateTime = '" & MatchDate & "', Venue = '" & Venue.Replace("'", "''").Trim() & "', Country = '" & Country.Replace("'", "''").Trim() & "', HomeTeamid = " & HomeTeamId & ", AwayTeamId = " & AwayTeamId & ", HomeTeamName = '" & HomeTeamName.Replace("'", "''").Trim() & "', AwayTeamName = '" & AwayTeamName.Replace("'", "''").Trim() & "', Status = '" & Status.Replace("'", "''").Trim() & "', StatusId = " & StatusId & ", Modified = '" & DateTime.Now & "' Where (Id = '" & Id & "')", DatabaseConn)
                                        Else
                                            SqlQuery = New SqlCommand("Update Rugby.dbo.pa_Matches Set Competition = '" & Competition.Replace("'", "''").Trim() & "', Season = " & Season & ", Stage = " & Stage & ", Round = " & Round & ", Leg = " & Leg & ", Replay = " & Replay & ", PoolsNo = '" & PoolsNo.Replace("'", "''").Trim() & "', MatchDateTime = '" & MatchDate & "', Venue = '" & Venue.Replace("'", "''").Trim() & "', Country = '" & Country.Replace("'", "''").Trim() & "', HomeTeamid = " & HomeTeamId & ", AwayTeamId = " & AwayTeamId & ", HomeTeamName = '" & HomeTeamName.Replace("'", "''").Trim() & "', AwayTeamName = '" & AwayTeamName.Replace("'", "''").Trim() & "', Modified = '" & DateTime.Now & "' Where (Id = '" & Id & "')", DatabaseConn)
                                        End If
                                        If Convert.ToInt32(Competition) <> 602 Then
                                            SqlQuery.ExecuteNonQuery()
                                        End If
                                    Else
                                        SqlQuery = New SqlCommand("Insert Into Rugby.dbo.pa_Matches (Id, Season, Competition, Stage, Round, Leg, Replay, PoolsNo, MatchDateTime, Venue, Country, HomeTeamId, AwayTeamId, HomeTeamName, AwayTeamName, Status, StatusId, Modified, Created) Values ('" & Id & "', " & Season & ", '" & Competition.Replace("'", "''").Trim() & "', " & Stage & ", " & Round & ", " & Leg & ", " & Replay & ", '" & PoolsNo.Replace("'", "''").Trim() & "', '" & MatchDate & "', '" & Venue.Replace("'", "''").Trim() & "', '" & Country.Replace("'", "''").Trim() & "', " & HomeTeamId & ", " & AwayTeamId & ", '" & HomeTeamName.Replace("'", "''").Trim() & "', '" & AwayTeamName.Replace("'", "''").Trim() & "', '" & Status.Replace("'", "''").Trim() & "', " & StatusId & ", '" & DateTime.Now & "', '" & DateTime.Now & "')", DatabaseConn)
                                        SqlQuery.ExecuteNonQuery()
                                    End If
                                Else
                                    MoveFile = False
                                    DatabaseError = True
                                End If
                            Next
                            xmlMatches = Nothing
                            xmlDoc = Nothing
                        Catch ex As Exception
                            MoveFile = False
                            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                                DatabaseError = True
                            Else
                                Call ErrorHandler(MatchId, ex.Source & "-" & ex.Message, "Error with the fixture")
                            End If
                        End Try
                        If MoveFile = True Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture\Complete\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf DatabaseError = False Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        End If
                    End If
                Next

                updateListbox("Fixtures complete", True)
            End If
        End If

    End Sub

    Private Sub FixturesTT()

        Dim File As String
        Dim Files() As String
        Dim Fi As FileInfo
        Dim MatchId As String = ""
        Dim DbCount As Integer = 0
        Dim DatabaseError As Boolean = False

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture TT\Temporary") Then
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture TT\Temporary")

            Dim Count As Integer = 0
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf(".xml") >= 0 Then
                    Count = Count + 1
                End If
            Next

            If Count > 0 Then
                updateListbox("Processing TT fixtures", True)

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And DbConnected = True Then
                        Dim MoveFile As Boolean = True
                        Dim fileStream As FileStream
                        Dim sr As StreamReader
                        Dim xmlDoc As XmlDocument
                        Try
                            fileStream = New FileStream(File, FileMode.Open, FileAccess.ReadWrite)
                            sr = New StreamReader(fileStream)
                            xmlDoc = New XmlDocument
                            xmlDoc.Load(sr)

                            Dim xmlNode As XmlNode
                            For Each xmlNode In xmlDoc.SelectSingleNode("XML").ChildNodes
                                Select Case xmlNode.Name
                                    Case "DAY"
                                        Dim stringDate As String() = xmlNode.Attributes("DATE").InnerText.Split("/")
                                        Dim currentDate As DateTime = New DateTime(Convert.ToInt32(stringDate(2)), Convert.ToInt32(stringDate(1)), Convert.ToInt32(stringDate(0)))
                                        Dim xmlCompetition As XmlNode
                                        For Each xmlCompetition In xmlNode.SelectNodes("SPORT/COMPETITION")
                                            Dim Competition As Integer = Convert.ToInt32(xmlCompetition.Attributes("UID").InnerText)
                                            Dim Season As Integer = 1
                                            SqlQuery = New SqlCommand("SELECT TOP 1 Competition_Id, Competition_Season FROM rugby.dbo.pa_competitions WHERE (leagueId = @id) ORDER BY Competition_Season DESC", DatabaseConn)
                                            SqlQuery.Parameters.Add("@id", SqlDbType.Int).Value = Competition
                                            Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader()
                                            While RsRec.Read
                                                Competition = Convert.ToInt32(RsRec("Competition_Id"))
                                                Season = Convert.ToInt32(RsRec("Competition_Season"))
                                            End While
                                            RsRec.Close()

                                            Dim xmlRound As XmlNode
                                            For Each xmlRound In xmlCompetition.SelectNodes("ROUND")
                                                Dim xmlMatch As XmlNode
                                                For Each xmlMatch In xmlRound.SelectNodes("MATCH")
                                                    Dim homeTeamId As Integer
                                                    Dim awayTeamId As Integer
                                                    Dim homeTeamName As String = ""
                                                    Dim awayTeamName As String = ""
                                                    Dim venue As String = ""
                                                    Dim matchTime As String()
                                                    Dim matchDateTime As DateTime

                                                    MatchId = Convert.ToInt32(xmlMatch.Attributes("MATCHID").InnerText)

                                                    Dim xmlDetail As XmlNode
                                                    For Each xmlDetail In xmlMatch.ChildNodes
                                                        Select Case xmlDetail.Name
                                                            Case "HOMETEAM"
                                                                homeTeamId = Convert.ToInt32(xmlDetail.Attributes("TEAMID").InnerText)
                                                                homeTeamName = xmlDetail.InnerText
                                                            Case "AWAYTEAM"
                                                                awayTeamId = Convert.ToInt32(xmlDetail.Attributes("TEAMID").InnerText)
                                                                awayTeamName = xmlDetail.InnerText
                                                            Case "VENUE"
                                                                venue = xmlDetail.InnerText
                                                            Case "STARTTIME"
                                                                If xmlDetail.InnerText.IndexOf(":") >= 0 Then
                                                                    matchTime = xmlDetail.InnerText.Split(":")
                                                                    matchDateTime = New DateTime(currentDate.Year, currentDate.Month, currentDate.Day, Convert.ToInt32(matchTime(0)), Convert.ToInt32(matchTime(1)), 0)
                                                                Else
                                                                    matchDateTime = New DateTime(currentDate.Year, currentDate.Month, currentDate.Day, 0, 0, 0)
                                                                End If
                                                        End Select
                                                    Next

                                                    SqlQuery = New SqlCommand("SELECT COUNT(Id) FROM rugby.dbo.pa_Matches WHERE (Id = @id)", DatabaseConn)
                                                    SqlQuery.Parameters.Add("@id", SqlDbType.VarChar).Value = MatchId
                                                    Dim Present As Integer = Convert.ToInt32(SqlQuery.ExecuteScalar)

                                                    If Present > 0 Then
                                                        SqlQuery = New SqlCommand("Execute rugby.dbo.pa_SetTeamNames " & Competition & ", " & Season & ", " & homeTeamId & ", '" & homeTeamName.Replace("'", "''") & "'", DatabaseConn)
                                                        SqlQuery.ExecuteNonQuery()

                                                        SqlQuery = New SqlCommand("Execute rugby.dbo.pa_SetTeamNames " & Competition & ", " & Season & ", " & awayTeamId & ", '" & awayTeamName.Replace("'", "''") & "'", DatabaseConn)
                                                        SqlQuery.ExecuteNonQuery()

                                                        SqlQuery = New SqlCommand("UPDATE Rugby.dbo.pa_Matches Set MatchDateTime = @matchDateTime, Venue = @venue, HomeTeamId = @homeTeamId, AwayTeamId = @awayTeamId, HomeTeamName = @homeTeamName, AwayTeamName = @awayTeamName, Modified = GetDate() WHERE Id = @id;", DatabaseConn)
                                                        SqlQuery.Parameters.Add("@id", SqlDbType.VarChar).Value = MatchId
                                                        SqlQuery.Parameters.Add("@matchDateTime", SqlDbType.VarChar).Value = matchDateTime
                                                        SqlQuery.Parameters.Add("@venue", SqlDbType.VarChar).Value = venue
                                                        SqlQuery.Parameters.Add("@homeTeamId", SqlDbType.Int).Value = homeTeamId
                                                        SqlQuery.Parameters.Add("@homeTeamName", SqlDbType.VarChar).Value = homeTeamName
                                                        SqlQuery.Parameters.Add("@awayTeamId", SqlDbType.Int).Value = awayTeamId
                                                        SqlQuery.Parameters.Add("@awayTeamName", SqlDbType.VarChar).Value = awayTeamName
                                                        SqlQuery.ExecuteNonQuery()
                                                    Else
                                                        SqlQuery = New SqlCommand("Execute rugby.dbo.pa_SetTeamNames " & Competition & ", " & Season & ", " & homeTeamId & ", '" & homeTeamName.Replace("'", "''") & "'", DatabaseConn)
                                                        SqlQuery.ExecuteNonQuery()

                                                        SqlQuery = New SqlCommand("Execute rugby.dbo.pa_SetTeamNames " & Competition & ", " & Season & ", " & awayTeamId & ", '" & awayTeamName.Replace("'", "''") & "'", DatabaseConn)
                                                        SqlQuery.ExecuteNonQuery()

                                                        SqlQuery = New SqlCommand("INSERT INTO Rugby.dbo.pa_Matches (Id, Season, Competition, Stage, Round, Leg, Replay, PoolsNo, MatchDateTime, Venue, Country, HomeTeamId, AwayTeamId, HomeTeamName, AwayTeamName, Status, StatusId, Modified, Created) Values (@id, @season, @competition, 1, 1, 1, 0, '', @matchDateTime, @venue, '', @homeTeamId, @awayTeamId, @homeTeamName, @awayTeamName, '', 0, GetDate(), GetDate());", DatabaseConn)
                                                        SqlQuery.Parameters.Add("@id", SqlDbType.VarChar).Value = MatchId
                                                        SqlQuery.Parameters.Add("@season", SqlDbType.Int).Value = Season
                                                        SqlQuery.Parameters.Add("@competition", SqlDbType.Int).Value = Competition
                                                        SqlQuery.Parameters.Add("@matchDateTime", SqlDbType.VarChar).Value = matchDateTime
                                                        SqlQuery.Parameters.Add("@venue", SqlDbType.VarChar).Value = venue
                                                        SqlQuery.Parameters.Add("@homeTeamId", SqlDbType.Int).Value = homeTeamId
                                                        SqlQuery.Parameters.Add("@homeTeamName", SqlDbType.VarChar).Value = homeTeamName
                                                        SqlQuery.Parameters.Add("@awayTeamId", SqlDbType.Int).Value = awayTeamId
                                                        SqlQuery.Parameters.Add("@awayTeamName", SqlDbType.VarChar).Value = awayTeamName
                                                        SqlQuery.ExecuteNonQuery()
                                                    End If
                                                Next
                                            Next
                                        Next
                                    Case "CORRECTIONS"
                                        Dim xmlAttribute As XmlAttribute
                                        Dim type As String
                                        Dim id As String
                                        For Each xmlAttribute In xmlNode.Attributes
                                            If xmlAttribute.Name = "TYPE" Then
                                                type = xmlAttribute.InnerText
                                            End If
                                            If xmlAttribute.Name = "MATCHID" Then
                                                id = xmlAttribute.InnerText
                                            End If
                                        Next

                                        If type = "DELETE" And Not String.IsNullOrEmpty(id) Then
                                            Try
                                                SqlQuery = New SqlCommand("DELETE FROM Rugby.dbo.pa_Matches WHERE (Id = @matchId)", DatabaseConn)
                                                SqlQuery.Parameters.Add("@matchId", SqlDbType.VarChar).Value = Convert.ToInt32(id)
                                                SqlQuery.ExecuteNonQuery()
                                            Catch ex As Exception

                                            End Try
                                        End If
                                End Select
                            Next
                        Catch ex As Exception
                            MoveFile = False
                            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                                DatabaseError = True
                            Else
                                Call ErrorHandler(MatchId, ex.Source & "-" & ex.Message, "Error with the TT fixture")
                            End If
                        Finally
                            xmlDoc = Nothing
                            sr.Close()
                            fileStream.Dispose()
                        End Try
                        If MoveFile = True Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture TT\Complete\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf DatabaseError = False Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture TT\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        End If
                    End If
                Next

                updateListbox("TT Fixtures complete", True)
            End If
        End If

    End Sub

#End Region

#Region " Matches "

    Private Sub Matches()

        Dim File As String
        Dim Files() As String
        Dim Fi As FileInfo
        Dim MatchId As String = ""

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Match\Temporary") Then
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Match\Temporary")

            Dim Count As Integer = 0
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf(".xml") >= 0 Then
                    Count = Count + 1
                End If
            Next
            If Count > 0 Then
                updateListbox("Processing matches", True)

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And DbConnected = True Then
                        Dim MoveFile As Integer = 1
                        Try
                            Dim xmlDoc As New XmlDocument
                            xmlDoc.Load(File)

                            Dim xmlEvents As XmlNodeList = xmlDoc.SelectNodes("RugbyMatch")
                            Dim I As Integer
                            For I = 0 To xmlEvents.Count - 1
                                Dim Revision As Integer = 0
                                Dim Attendance As String = ""
                                Dim Comments As String = ""
                                Dim Status As String = ""
                                Dim StatusId As Integer = 0
                                Dim Result As Integer = 0

                                Dim xmlDetails As XmlAttributeCollection = xmlEvents(I).Attributes
                                Dim N As Integer
                                For N = 0 To xmlDetails.Count - 1
                                    Dim tmpName As String = xmlDetails(N).Name
                                    Select Case tmpName
                                        Case "revision"
                                            Revision = xmlDetails(N).InnerText
                                        Case "id"
                                            MatchId = xmlDetails(N).InnerText
                                        Case "attendance"
                                            Attendance = xmlDetails(N).InnerText
                                        Case "status"
                                            Status = xmlDetails(N).InnerText
                                            StatusId = retStatus(Status.ToLower)
                                        Case "comment"
                                            Comments = xmlDetails(N).InnerText
                                        Case Else

                                    End Select
                                Next

                                SqlQuery = New SqlCommand("Select Revision From Rugby.dbo.pa_Matches Where (Id = '" & MatchId & "')", DatabaseConn)
                                Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
                                Dim tmpRevision As Integer = 1000
                                If RsRec.HasRows Then
                                    While RsRec.Read
                                        tmpRevision = RsRec("Revision")
                                    End While
                                Else

                                End If
                                RsRec.Close()

                                If Revision >= tmpRevision Then
                                    If StatusId = 8 Then
                                        StatusId = 14
                                        Status = "Result"
                                    End If
                                    If StatusId = 14 Then
                                        Result = 1
                                    End If
                                    SqlQuery = New SqlCommand("Update Rugby.dbo.pa_Matches Set Revision = " & Revision & ", Attendance = '" & Attendance.Replace("'", "''") & "', Status = '" & Status.Replace("'", "''") & "', StatusId = " & StatusId & ", Comment = '" & Comments.Replace("'", "''") & "', Result = " & Result & " Where (Id = '" & MatchId & "')", DatabaseConn)
                                    SqlQuery.ExecuteNonQuery()

                                    Dim xmlTypes As XmlNodeList = xmlEvents(I).ChildNodes
                                    For N = 0 To xmlTypes.Count - 1
                                        Dim Type As String = xmlTypes(N).Name
                                        If DbConnected = True Then
                                            If Type = "Team" Then
                                                MoveFile = Teams(MatchId, xmlTypes(N))
                                            ElseIf Type = "Officials" Then
                                                MoveFile = Officials(MatchId, xmlTypes(N))
                                            ElseIf Type = "Bookings" Then
                                                MoveFile = Booking(MatchId, xmlTypes(N))
                                            ElseIf Type = "Replacements" Then
                                                MoveFile = Substitution(MatchId, xmlTypes(N))
                                            ElseIf Type = "Scoring" Then
                                                MoveFile = Scoring(MatchId, xmlTypes(N))
                                            ElseIf Type = "Penalties" Then

                                            Else
                                                MoveFile = 2
                                            End If

                                            If MoveFile = 0 Then
                                                updateListbox(Type, True)
                                            End If
                                        Else
                                            MoveFile = 3
                                        End If
                                        If MoveFile > 1 Then
                                            Exit For
                                        End If
                                    Next
                                    xmlTypes = Nothing
                                End If
                            Next
                            xmlEvents = Nothing
                            xmlDoc = Nothing
                        Catch ex As Exception
                            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                                MoveFile = 3
                                'Call ErrorHandler(MatchId, ex.Message, "Error with the matches")
                            Else
                                MoveFile = 2
                                Call ErrorHandler(MatchId, ex.Message, "Error with the matches")
                            End If
                        End Try
                        If MoveFile = 1 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Match\Complete\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf MoveFile = 2 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Match\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        End If
                    End If
                Next

                updateListbox("Matches complete", True)
            End If
        End If

    End Sub

    Private Function Teams(ByVal MatchId As String, ByVal xmlTeam As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim TeamId As Integer
        Dim TeamName As String = ""
        Dim HomeTeam As Integer = 0
        Dim QueryText As String = ""

        Try
            Dim tmpNodes As XmlAttributeCollection = xmlTeam.Attributes
            Dim M As Integer
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        TeamId = tmpNodes(M).InnerText
                    Case "name"
                        TeamName = tmpNodes(M).InnerText
                    Case "homeOrAway"
                        If tmpNodes(M).InnerText = "Home" Then
                            HomeTeam = 1
                        End If
                    Case Else

                End Select
            Next

            Dim xmlPlayers As XmlNodeList = xmlTeam.SelectNodes("Players/Player")
            Dim I As Integer
            For I = 0 To xmlPlayers.Count - 1
                Dim Id As Integer = xmlPlayers(I).Attributes.ItemOf("id").InnerText
                Dim SquadNumber As Integer = 0
                Dim Position As String = ""
                Dim Substitute As Integer = 0
                Dim Captain As Integer = 0
                tmpNodes = xmlPlayers(I).Attributes
                For M = 0 To tmpNodes.Count - 1
                    Dim tmpName As String = tmpNodes(M).Name
                    Select Case tmpName
                        Case "id"
                            Id = tmpNodes(M).InnerText
                        Case "squadNumber"
                            SquadNumber = tmpNodes(M).InnerText
                        Case "position"
                            Position = tmpNodes(M).InnerText
                        Case "substitute"
                            If tmpNodes(M).InnerText = "Yes" Then
                                Substitute = 1
                            End If
                        Case "captain"
                            If tmpNodes(M).InnerText = "Yes" Then
                                Captain = 1
                            End If
                        Case Else

                    End Select
                Next

                Dim FirstName As String = ""
                Dim Initials As String = ""
                Dim LastName As String = ""
                Dim FullName As String = xmlPlayers(I).SelectSingleNode("Name").InnerText
                tmpNodes = xmlPlayers(I).SelectSingleNode("Name").Attributes
                For M = 0 To tmpNodes.Count - 1
                    Dim tmpName As String = tmpNodes(M).Name
                    Select Case tmpName
                        Case "firstName"
                            FirstName = tmpNodes(M).InnerText
                        Case "initials"
                            Initials = tmpNodes(M).InnerText
                        Case "lastName"
                            LastName = tmpNodes(M).InnerText
                        Case Else

                    End Select
                Next

                If I = 0 Then
                    QueryText = "Insert Into Rugby.dbo.pa_Teams (MatchId, TeamId, TeamName, HomeTeam, PlayerId, PlayerFirstName, PlayerLastname, PlayerInitials, PlayerFullName, Position, Substitute, Captain, SquadNumber, Sorting, Modified, Created) Values ('" & MatchId & "', " & TeamId & ", '" & TeamName.Replace("'", "''") & "', " & HomeTeam & ", " & Id & ", '" & FirstName.Replace("'", "''") & "', '" & LastName.Replace("'", "''") & "', '" & Initials.Replace("'", "''") & "', '" & FullName.Replace("'", "''") & "', '" & Position.Replace("'", "''") & "', " & Substitute & ", " & Captain & ", " & SquadNumber & ", " & I & ", '" & DateTime.Now() & "', '" & DateTime.Now() & "')"
                Else
                    QueryText &= ";" & Environment.NewLine & "Insert Into Rugby.dbo.pa_Teams (MatchId, TeamId, TeamName, HomeTeam, PlayerId, PlayerFirstName, PlayerLastname, PlayerInitials, PlayerFullName, Position, Substitute, Captain, SquadNumber, Sorting, Modified, Created) Values ('" & MatchId & "', " & TeamId & ", '" & TeamName.Replace("'", "''") & "', " & HomeTeam & ", " & Id & ", '" & FirstName.Replace("'", "''") & "', '" & LastName.Replace("'", "''") & "', '" & Initials.Replace("'", "''") & "', '" & FullName.Replace("'", "''") & "', '" & Position.Replace("'", "''") & "', " & Substitute & ", " & Captain & ", " & SquadNumber & ", " & I & ", '" & DateTime.Now() & "', '" & DateTime.Now() & "')"
                End If
            Next

            If QueryText <> "" Then
                If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                    SqlQuery = New SqlCommand("Delete From Rugby.dbo.pa_Teams Where (MatchId = '" & MatchId & "') And (TeamId = " & TeamId & ")", DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                    SqlQuery = New SqlCommand(QueryText, DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                Else
                    Completed = 3
                End If
            End If
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler(MatchId, ex.Message, "Error with the team")
            End If
        End Try

        Return Completed

    End Function

    Private Function Officials(ByVal MatchId As String, ByVal xmlOfficials As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim QueryText As String = ""

        Try
            Dim tmpNodeList As XmlNodeList = xmlOfficials.SelectNodes("Official")
            Dim I As Integer
            For I = 0 To tmpNodeList.Count - 1
                Dim Id As Integer = 0
                Dim Type As String = ""
                Dim POB As String = ""

                Dim tmpNodes As XmlAttributeCollection = tmpNodeList(I).Attributes
                Dim M As Integer
                For M = 0 To tmpNodes.Count - 1
                    Dim tmpName As String = tmpNodes(M).Name
                    Select Case tmpName
                        Case "id"
                            Id = tmpNodes(M).InnerText
                        Case Else

                    End Select
                Next

                Dim Initials As String = ""
                Dim LastName As String = ""
                Dim FirstName As String = ""
                Dim FullName As String = tmpNodeList(I).SelectSingleNode("Name").InnerText
                tmpNodes = tmpNodeList(I).SelectSingleNode("Name").Attributes
                For M = 0 To tmpNodes.Count - 1
                    Dim tmpName As String = tmpNodes(M).Name
                    Select Case tmpName
                        Case "initials"
                            Initials = tmpNodes(M).InnerText
                        Case "lastName"
                            LastName = tmpNodes(M).InnerText
                        Case "firstName"
                            FirstName = tmpNodes(M).InnerText
                        Case Else

                    End Select
                Next

                If I = 0 Then
                    QueryText = "Insert Into Rugby.dbo.pa_Officials (MatchId, Id, Initials, FirstName, LastName, FullName, Modified, Created) Values ('" & MatchId & "', " & Id & ", '" & Initials & "', '" & FirstName.Replace("'", "''") & "', '" & LastName.Replace("'", "''") & "', '" & FullName.Replace("'", "''") & "', '" & DateTime.Now() & "', '" & DateTime.Now() & "')"
                Else
                    QueryText &= ";" & Environment.NewLine & "Insert Into Rugby.dbo.pa_Officials (MatchId, Id, Initials, FirstName, LastName, FullName, Modified, Created) Values ('" & MatchId & "', " & Id & ", '" & Initials & "', '" & FirstName.Replace("'", "''") & "', '" & LastName.Replace("'", "''") & "', '" & FullName.Replace("'", "''") & "', '" & DateTime.Now() & "', '" & DateTime.Now() & "')"
                End If
            Next

            If QueryText <> "" Then
                If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                    SqlQuery = New SqlCommand("Delete From Rugby.dbo.pa_Officials Where (MatchId = '" & MatchId & "')", DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                    SqlQuery = New SqlCommand(QueryText, DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                Else
                    Completed = 3
                End If
            End If
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler(MatchId, ex.Message, "Error with the officials")
            End If
        End Try

        Return Completed

    End Function

    Private Function Substitution(ByVal MatchId As String, ByVal xmlReplacements As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim QueryText As String = ""

        Try
            Dim tmpNodeList As XmlNodeList = xmlReplacements.SelectNodes("Replacement")
            Dim I As Integer
            For I = 0 To tmpNodeList.Count - 1
                Dim teamId As Integer = 0
                Dim offId As Integer = 0
                Dim onId As Integer = 0
                Dim Minutes As Integer = 0
                Dim Substitute As Integer = 0

                Dim tmpNodes As XmlAttributeCollection = tmpNodeList(I).Attributes
                Dim M As Integer
                For M = 0 To tmpNodes.Count - 1
                    Dim tmpName As String = tmpNodes(M).Name
                    Select Case tmpName
                        Case "teamId"
                            teamId = tmpNodes(M).InnerText
                        Case "offId"
                            offId = tmpNodes(M).InnerText
                        Case "onId"
                            onId = tmpNodes(M).InnerText
                        Case "minutes"
                            Minutes = tmpNodes(M).InnerText
                        Case "substitute"
                            If tmpNodes(M).InnerText = "Yes" Then
                                Substitute = 1
                            End If
                        Case Else

                    End Select
                Next

                If I = 0 Then
                    QueryText = "Insert Into Rugby.dbo.pa_Substitutions (MatchId, TeamId, OffId, OnId, Minutes, Substitution, Created) Values ('" & MatchId & "', " & teamId & ", " & offId & ", " & onId & ", " & Minutes & ", " & Substitute & ", '" & DateTime.Now() & "')"
                Else
                    QueryText &= ";" & Environment.NewLine & "Insert Into Rugby.dbo.pa_Substitutions (MatchId, TeamId, OffId, OnId, Minutes, Substitution, Created) Values ('" & MatchId & "', " & teamId & ", " & offId & ", " & onId & ", " & Minutes & ", " & Substitute & ", '" & DateTime.Now() & "')"
                End If
            Next

            If QueryText <> "" Then
                If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                    SqlQuery = New SqlCommand("Delete From Rugby.dbo.pa_Substitutions Where (MatchId = '" & MatchId & "')", DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                    SqlQuery = New SqlCommand(QueryText, DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                Else
                    Completed = 3
                End If
            End If
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler(MatchId, ex.Message, "Error with the substitutions")
            End If
        End Try

        Return Completed

    End Function

    Private Function Booking(ByVal MatchId As String, ByVal xmlBookings As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim QueryText As String = ""

        Try
            Dim tmpNodeList As XmlNodeList = xmlBookings.SelectNodes("Booking")
            Dim I As Integer
            For I = 0 To tmpNodeList.Count - 1
                Dim teamId As Integer = 0
                Dim bookedId As Integer = 0
                Dim Card As String = ""
                Dim Minutes As Integer = 0
                Dim sinBin As Integer = 0

                Dim tmpNodes As XmlAttributeCollection = tmpNodeList(I).Attributes
                Dim M As Integer
                For M = 0 To tmpNodes.Count - 1
                    Dim tmpName As String = tmpNodes(M).Name
                    Select Case tmpName
                        Case "teamId"
                            teamId = tmpNodes(M).InnerText
                        Case "bookedId"
                            bookedId = tmpNodes(M).InnerText
                        Case "card"
                            Card = tmpNodes(M).InnerText
                        Case "minutes"
                            Minutes = tmpNodes(M).InnerText
                        Case "sinBin"
                            If tmpNodes(M).InnerText = "Yes" Then
                                sinBin = 1
                            End If
                        Case Else

                    End Select
                Next

                If I = 0 Then
                    QueryText = "Insert Into Rugby.dbo.pa_Bookings (MatchId, TeamId, BookedId, Card, Minutes, SinBin, Created) Values ('" & MatchId & "', " & teamId & ", " & bookedId & ", '" & Card.Replace("'", "''") & "', " & Minutes & ", " & sinBin & ", '" & DateTime.Now() & "')"
                Else
                    QueryText &= ";" & Environment.NewLine & "Insert Into Rugby.dbo.pa_Bookings (MatchId, TeamId, BookedId, Card, Minutes, SinBin, Created) Values ('" & MatchId & "', " & teamId & ", " & bookedId & ", '" & Card.Replace("'", "''") & "', " & Minutes & ", " & sinBin & ", '" & DateTime.Now() & "')"
                End If
            Next

            If QueryText <> "" Then
                If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                    SqlQuery = New SqlCommand("Delete From Rugby.dbo.pa_Bookings Where (MatchId = '" & MatchId & "')", DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                    SqlQuery = New SqlCommand(QueryText, DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                Else
                    Completed = 3
                End If
            End If
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler(MatchId, ex.Message, "Error with the bookings")
            End If
        End Try

        Return Completed

    End Function

    Private Function Scoring(ByVal MatchId As String, ByVal xmlScoring As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim QueryText As String = ""

        Try
            Dim HomeTeamId As Integer = 0
            Dim AwayTeamId As Integer = 0
            SqlQuery = New SqlCommand("Select HomeTeamId, AwayTeamId From Rugby.dbo.pa_Matches Where (Id = '" & MatchId & "')", DatabaseConn)
            Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
            While RsRec.Read
                HomeTeamId = RsRec("HomeTeamId")
                AwayTeamId = RsRec("AwayTeamId")
            End While
            RsRec.Close()

            Dim xmlPoints As XmlNodeList = xmlScoring.SelectNodes("Points")
            Dim I As Integer
            For I = 0 To xmlPoints.Count - 1
                Dim TeamId As Integer = 0
                Dim Total As Integer = 0
                Dim Twenty As Integer = -1
                Dim HalfTime As Integer = -1
                Dim Sixty As Integer = -1
                Dim FullTime As Integer = -1
                Dim ExtraTime As Integer = -1

                Dim tmpNodes As XmlAttributeCollection = xmlPoints(I).Attributes
                Dim M As Integer
                For M = 0 To tmpNodes.Count - 1
                    Dim tmpName As String = tmpNodes(M).Name
                    Dim tmpValue As String = tmpNodes(M).InnerText
                    Select Case tmpName
                        Case "teamId"
                            TeamId = tmpNodes(M).InnerText
                        Case "total"
                            If IsNumeric(tmpValue) Then
                                Total = tmpNodes(M).InnerText
                            End If
                        Case "twentyMinutes"
                            If IsNumeric(tmpValue) Then
                                Twenty = tmpNodes(M).InnerText
                            End If
                        Case "extraTime"
                            If IsNumeric(tmpValue) Then
                                ExtraTime = tmpNodes(M).InnerText
                            End If
                        Case "halfTime"
                            If IsNumeric(tmpValue) Then
                                HalfTime = tmpNodes(M).InnerText
                            End If
                        Case "sixtyMinutes"
                            If IsNumeric(tmpValue) Then
                                Sixty = tmpNodes(M).InnerText
                            End If
                        Case "fullTime"
                            If IsNumeric(tmpValue) Then
                                FullTime = tmpNodes(M).InnerText
                            End If
                        Case Else

                    End Select
                Next

                If QueryText <> "" Then
                    QueryText &= Environment.NewLine
                End If

                If TeamId = HomeTeamId Then
                    QueryText &= "Update Rugby.dbo.pa_Matches Set HomeTeamScore = " & Total & ", twentyMinuteScoreHT = " & Twenty & ", HalfTimeHomeTeamScore = " & HalfTime & ", sixtyMinuteScoreHT = " & Sixty & ", fullTimeScoreHT = " & FullTime & ", extraTimeScoreHT = " & ExtraTime & " Where (Id = '" & MatchId & "');"
                ElseIf TeamId = AwayTeamId Then
                    QueryText &= "Update Rugby.dbo.pa_Matches Set AwayTeamScore = " & Total & ", twentyMinuteScoreAT = " & Twenty & ", HalfTimeAwayTeamScore = " & HalfTime & ", sixtyMinuteScoreAT = " & Sixty & ", fullTimeScoreAT = " & FullTime & ", extraTimeScoreAT = " & ExtraTime & " Where (Id = '" & MatchId & "');"
                End If
            Next

            Dim xmlScores As XmlNodeList = xmlScoring.SelectNodes("Score")
            For I = 0 To xmlScores.Count - 1
                Dim TeamId As Integer = 0
                Dim Type As String = ""
                Dim ScorerId As Integer = 0
                Dim Minutes As Integer = 0
                Dim FromPenalty As Integer = 0
                Dim tmpNodes As XmlAttributeCollection = xmlScores(I).Attributes
                Dim M As Integer
                For M = 0 To tmpNodes.Count - 1
                    Dim tmpName As String = tmpNodes(M).Name
                    Dim tmpValue As String = tmpNodes(M).InnerText
                    Select Case tmpName
                        Case "type"
                            Type = tmpNodes(M).InnerText
                        Case "teamId"
                            TeamId = tmpNodes(M).InnerText
                        Case "scorerId"
                            ScorerId = tmpNodes(M).InnerText
                        Case "minutes"
                            If tmpNodes(M).InnerText <> "" Then
                                Minutes = tmpNodes(M).InnerText
                            End If
                        Case "fromPenalty"
                            If tmpNodes(M).InnerText = "Yes" Then
                                FromPenalty = 1
                            End If
                        Case Else

                    End Select
                Next

                If QueryText <> "" Then
                    QueryText &= Environment.NewLine
                End If

                QueryText &= "Insert Into Rugby.dbo.pa_Scoring (MatchId, TeamId, Type, ScorerId, Minutes, FromPenalty, Sorting, Created) Values ('" & MatchId & "', " & TeamId & ", '" & Type.Replace("'", "''") & "', " & ScorerId & ", " & Minutes & ", " & FromPenalty & ", " & I & ", '" & DateTime.Now & "');"
            Next

            If QueryText <> "" Then
                If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                    SqlQuery = New SqlCommand("Delete From Rugby.dbo.pa_Scoring Where (MatchId = '" & MatchId & "');", DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                    SqlQuery = New SqlCommand(QueryText, DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                Else
                    Completed = 3
                End If
            End If
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler(MatchId, ex.Message, "Error with the scoring")
            End If
        End Try

        Return Completed

    End Function

    Private Sub ResultsTT()

        Dim File As String
        Dim Files() As String
        Dim Fi As FileInfo
        Dim MatchId As String = ""
        Dim DbCount As Integer = 0
        Dim DatabaseError As Boolean = False

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Result TT\Temporary") Then
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Result TT\Temporary")

            Dim Count As Integer = 0
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf(".xml") >= 0 Then
                    Count = Count + 1
                End If
            Next

            If Count > 0 Then
                updateListbox("Processing TT results", True)

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And DbConnected = True Then
                        Dim MoveFile As Boolean = True
                        Dim fileStream As FileStream
                        Dim xmlDoc As XmlDocument
                        Dim sr As StreamReader
                        Try
                            fileStream = New FileStream(File, FileMode.Open, FileAccess.ReadWrite)
                            sr = New StreamReader(fileStream)
                            xmlDoc = New XmlDocument
                            xmlDoc.Load(sr)

                            Dim xmlNode As XmlNode
                            For Each xmlNode In xmlDoc.FirstChild.ChildNodes
                                Select Case xmlNode.Name
                                    Case "DAY"
                                        Dim xmlCompetition As XmlNode
                                        For Each xmlCompetition In xmlNode.SelectNodes("SPORT/COMPETITION")
                                            Dim Competition As Integer = Convert.ToInt32(xmlCompetition.Attributes("UID").InnerText)
                                            Dim Season As Integer = 1
                                            SqlQuery = New SqlCommand("SELECT TOP 1 Competition_Id, Competition_Season FROM rugby.dbo.pa_competitions WHERE (leagueId = @id) ORDER BY Competition_Season DESC", DatabaseConn)
                                            SqlQuery.Parameters.Add("@id", SqlDbType.Int).Value = Competition
                                            Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader()
                                            While RsRec.Read
                                                Competition = Convert.ToInt32(RsRec("Competition_Id"))
                                                Season = Convert.ToInt32(RsRec("Competition_Season"))
                                            End While
                                            RsRec.Close()

                                            Dim xmlRound As XmlNode
                                            For Each xmlRound In xmlCompetition.SelectNodes("ROUND")
                                                Dim xmlMatch As XmlNode
                                                For Each xmlMatch In xmlRound.SelectNodes("MATCH")
                                                    Dim homeTeamScore As Integer = 0
                                                    Dim awayTeamScore As Integer = 0

                                                    MatchId = Convert.ToInt32(xmlMatch.Attributes("MATCHID").InnerText)
                                                    If MatchId = 2016167 Then
                                                        MatchId = 3000001
                                                    End If

                                                    Dim xmlDetail As XmlNode
                                                    For Each xmlDetail In xmlMatch.ChildNodes
                                                        Select Case xmlDetail.Name
                                                            Case "HOMETEAMSCORE"
                                                                homeTeamScore = Convert.ToInt32(xmlDetail.InnerText)
                                                            Case "AWAYTEAMSCORE"
                                                                awayTeamScore = Convert.ToInt32(xmlDetail.InnerText)
                                                        End Select
                                                    Next

                                                    SqlQuery = New SqlCommand("UPDATE Rugby.dbo.pa_Matches Set HomeTeamScore = @homeTeamScore, AwayTeamScore = @awayTeamScore, Status = 'Result', StatusId = 14, Result = 1, Modified = GetDate() WHERE Id = @id;", DatabaseConn)
                                                    SqlQuery.Parameters.Add("@id", SqlDbType.VarChar).Value = MatchId
                                                    SqlQuery.Parameters.Add("@homeTeamScore", SqlDbType.Int).Value = homeTeamScore
                                                    SqlQuery.Parameters.Add("@awayTeamScore", SqlDbType.Int).Value = awayTeamScore
                                                    SqlQuery.ExecuteNonQuery()
                                                Next
                                            Next
                                        Next
                                End Select
                            Next
                        Catch ex As Exception
                            MoveFile = False
                            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                                DatabaseError = True
                            Else
                                Call ErrorHandler(MatchId, ex.Source & "-" & ex.Message, "Error with the TT result")
                            End If
                        Finally
                            xmlDoc = Nothing
                            sr.Close()
                            fileStream.Dispose()
                        End Try
                        If MoveFile = True Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Result TT\Complete\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf DatabaseError = False Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Result TT\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        End If
                    End If
                Next

                updateListbox("TT Results complete", True)
            End If
        End If

    End Sub

#End Region

#Region " Logs "

    Private Sub Logs()

        Dim File As String
        Dim Files() As String
        Dim Fi As FileInfo
        Dim QueryText As String = ""

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log\Temporary") Then
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log\Temporary")

            Dim Count As Integer = 0
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf(".xml") >= 0 Then
                    Count = Count + 1
                End If
            Next
            If Count > 0 Then
                updateListbox("Processing logs", True)

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And DbConnected = True Then
                        Dim MoveFile As Integer = 1
                        Try
                            Dim xmlDoc As New XmlDocument
                            xmlDoc.Load(File)
                            Dim xmlTeams As XmlNodeList = xmlDoc.SelectNodes("RugbyTables/Teams/Team")
                            Dim I As Integer
                            Dim Teams As Hashtable = New Hashtable
                            Teams.Add("", "")
                            For I = 0 To xmlTeams.Count - 1
                                Dim tmpId As String = xmlTeams(I).Attributes.ItemOf("id").InnerText
                                Dim tmpName As String = xmlTeams(I).Attributes.ItemOf("name").InnerText
                                Teams.Add(tmpId, tmpName)
                            Next
                            xmlTeams = Nothing

                            Dim xmlTables As XmlNodeList = xmlDoc.SelectNodes("RugbyTables/Table")
                            For I = 0 To xmlTables.Count - 1
                                Dim Competition As Integer = 0
                                Dim StageNumber As Integer = 0
                                Dim RoundNumber As Integer = 0
                                Dim Season As Integer = 0
                                QueryText = ""

                                Dim xmlDetails As XmlAttributeCollection = xmlTables(I).Attributes
                                Dim N As Integer
                                For N = 0 To xmlDetails.Count - 1
                                    Dim tmpName As String = xmlDetails(N).Name
                                    Select Case tmpName
                                        Case "competitionId"
                                            Competition = xmlDetails(N).InnerText
                                        Case "stageNumber"
                                            StageNumber = xmlDetails(N).InnerText
                                        Case "roundNumber"
                                            RoundNumber = xmlDetails(N).InnerText
                                        Case Else

                                    End Select
                                Next

                                SqlQuery = New SqlCommand("Select Top 1 Competition_Season From Rugby.dbo.pa_Competitions Where (Competition_Id = " & Competition & ") And (Stage_Number = " & StageNumber & ") And (Round_Number = " & RoundNumber & ") And (Running = 1) Order By Competition_Season Desc", DatabaseConn)
                                Season = SqlQuery.ExecuteScalar

                                Dim xmlTeamPositions As XmlNodeList = xmlTables(I).SelectNodes("TeamPosition")
                                For N = 0 To xmlTeamPositions.Count - 1
                                    Dim Rank As Integer = 0
                                    Dim TeamId As Integer = 0
                                    Dim TeamName As String = ""
                                    Dim Played As Integer = 0
                                    Dim Won As Integer = 0
                                    Dim Drawn As Integer = 0
                                    Dim Lost As Integer = 0
                                    Dim PointsFor As Integer = 0
                                    Dim PointsAgainst As Integer = 0
                                    Dim PointsDifference As Integer = 0
                                    Dim BonusPoints As Integer = 0
                                    Dim Points As Integer = 0

                                    Dim PlayedH As Integer = 0
                                    Dim WonH As Integer = 0
                                    Dim DrawnH As Integer = 0
                                    Dim LostH As Integer = 0
                                    Dim PointsForH As Integer = 0
                                    Dim PointsAgainstH As Integer = 0

                                    Dim PlayedA As Integer = 0
                                    Dim WonA As Integer = 0
                                    Dim DrawnA As Integer = 0
                                    Dim LostA As Integer = 0
                                    Dim PointsForA As Integer = 0
                                    Dim PointsAgainstA As Integer = 0

                                    Dim xmlStats As XmlAttributeCollection = xmlTeamPositions(N).Attributes
                                    Dim M As Integer
                                    For M = 0 To xmlStats.Count - 1
                                        Dim tmpName As String = xmlStats(M).Name
                                        Select Case tmpName
                                            Case "rank"
                                                Rank = xmlStats(M).InnerText
                                            Case "teamId"
                                                TeamId = xmlStats(M).InnerText
                                                If Teams.ContainsKey(TeamId.ToString) Then
                                                    TeamName = Teams(TeamId.ToString)
                                                Else
                                                    TeamName = TeamId
                                                End If
                                            Case "played"
                                                Played = xmlStats(M).InnerText
                                            Case "won"
                                                Won = xmlStats(M).InnerText
                                            Case "drawn"
                                                Drawn = xmlStats(M).InnerText
                                            Case "lost"
                                                Lost = xmlStats(M).InnerText
                                            Case "for"
                                                PointsFor = xmlStats(M).InnerText
                                            Case "against"
                                                PointsAgainst = xmlStats(M).InnerText
                                            Case "pointDifference"
                                                PointsDifference = xmlStats(M).InnerText
                                            Case "bonus"
                                                BonusPoints = xmlStats(M).InnerText
                                            Case "points"
                                                Points = xmlStats(M).InnerText
                                            Case Else

                                        End Select
                                    Next

                                    xmlStats = xmlTeamPositions(N).SelectSingleNode("Home").Attributes
                                    For M = 0 To xmlStats.Count - 1
                                        Dim tmpName As String = xmlStats(M).Name
                                        Select Case tmpName
                                            Case "played"
                                                PlayedH = xmlStats(M).InnerText
                                            Case "won"
                                                WonH = xmlStats(M).InnerText
                                            Case "drawn"
                                                DrawnH = xmlStats(M).InnerText
                                            Case "lost"
                                                LostH = xmlStats(M).InnerText
                                            Case "for"
                                                PointsForH = xmlStats(M).InnerText
                                            Case "against"
                                                PointsAgainstH = xmlStats(M).InnerText
                                            Case Else

                                        End Select
                                    Next

                                    xmlStats = xmlTeamPositions(N).SelectSingleNode("Away").Attributes
                                    For M = 0 To xmlStats.Count - 1
                                        Dim tmpName As String = xmlStats(M).Name
                                        Select Case tmpName
                                            Case "played"
                                                PlayedA = xmlStats(M).InnerText
                                            Case "won"
                                                WonA = xmlStats(M).InnerText
                                            Case "drawn"
                                                DrawnA = xmlStats(M).InnerText
                                            Case "lost"
                                                LostA = xmlStats(M).InnerText
                                            Case "for"
                                                PointsForA = xmlStats(M).InnerText
                                            Case "against"
                                                PointsAgainstA = xmlStats(M).InnerText
                                            Case Else

                                        End Select
                                    Next

                                    If QueryText <> "" Then
                                        QueryText &= Environment.NewLine
                                    End If
                                    QueryText &= "Insert Into Rugby.dbo.pa_Logs (Competition, Season, Stage, Round, Rank, Sorting, TeamId, TeamName, Played, Won, Drawn, Lost, PointsFor, PointsAgainst, PointsDifference, BonusPoints, Points, PlayedH, WonH, DrawnH, LostH, PointsForH, PointsAgainstH, PlayedA, WonA, DrawnA, LostA, PointsForA, PointsAgainstA, Created) Values (" & Competition & ", " & Season & ", " & StageNumber & ", " & RoundNumber & ", " & Rank & ", " & N & ", " & TeamId & ", '" & TeamName.Replace("'", "''") & "', " & Played & ", " & Won & ", " & Drawn & ", " & Lost & ", " & PointsFor & ", " & PointsAgainst & ", " & PointsDifference & ", " & BonusPoints & ", " & Points & ", " & PlayedH & ", " & WonH & ", " & DrawnH & ", " & LostH & ", " & PointsForH & ", " & PointsAgainstH & ", " & PlayedA & ", " & WonA & ", " & DrawnA & ", " & LostA & ", " & PointsForA & ", " & PointsAgainstA & ", '" & DateTime.Now & "');"
                                Next

                                If QueryText <> "" Then
                                    If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                                        SqlQuery = New SqlCommand("Delete From Rugby.dbo.pa_Logs Where (Competition = '" & Competition & "') And (Round = " & RoundNumber & ") And (Stage = " & StageNumber & ")", DatabaseConn)
                                        SqlQuery.ExecuteNonQuery()
                                        SqlQuery = New SqlCommand(QueryText, DatabaseConn)
                                        SqlQuery.ExecuteNonQuery()
                                        'Dim Fs As New FileStream("C:\Program Files\SuperSport Zone\SuperSport Rugby\log.txt", FileMode.OpenOrCreate, FileAccess.Write)
                                        'Dim Sw As New StreamWriter(Fs)
                                        'Sw.BaseStream.Seek(0, SeekOrigin.End)
                                        'Sw.WriteLine(QueryText)
                                        'Sw.Close()
                                    End If
                                End If
                            Next
                            xmlTables = Nothing
                            xmlDoc = Nothing
                        Catch ex As Exception
                            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                                MoveFile = 3
                                Call ErrorHandler("", ex.Message, "Error with the logs")
                            Else
                                MoveFile = 2
                                Call ErrorHandler("", ex.Message, "Error with the logs")
                            End If
                        End Try
                        If MoveFile = 1 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log\Complete\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf MoveFile = 2 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        End If
                    End If
                Next

                updateListbox("Logs complete", True)
            End If
        End If

    End Sub

    Private Sub LogsTT()

        Dim File As String
        Dim Files() As String
        Dim Fi As FileInfo
        Dim MatchId As String = ""
        Dim DbCount As Integer = 0
        Dim DatabaseError As Boolean = False

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log TT\Temporary") Then
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log TT\Temporary")

            Dim Count As Integer = 0
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf(".xml") >= 0 Then
                    Count = Count + 1
                End If
            Next

            If Count > 0 Then
                updateListbox("Processing TT logs", True)

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And DbConnected = True Then
                        Dim MoveFile As Boolean = True
                        Dim fileStream As FileStream
                        Dim sr As StreamReader
                        Dim xmlDoc As XmlDocument
                        Try
                            fileStream = New FileStream(File, FileMode.Open, FileAccess.ReadWrite)
                            sr = New StreamReader(fileStream)
                            xmlDoc = New XmlDocument
                            Dim loaded As Boolean = True
                            Try
                                xmlDoc.Load(sr)
                            Catch ex As Exception
                                loaded = False
                            End Try

                            If loaded = True Then
                                Dim xmlNode As XmlNode
                                For Each xmlNode In xmlDoc.FirstChild.ChildNodes
                                    Select Case xmlNode.Name
                                        Case "COMPETITION"
                                            Dim sb As StringBuilder = New StringBuilder
                                            Dim Competition As Integer = Convert.ToInt32(xmlNode.Attributes("UID").InnerText)
                                            Dim Season As Integer = 1
                                            SqlQuery = New SqlCommand("SELECT TOP 1 Competition_Id, Competition_Season FROM rugby.dbo.pa_competitions WHERE (leagueId = @id) ORDER BY Competition_Season DESC", DatabaseConn)
                                            SqlQuery.Parameters.Add("@id", SqlDbType.Int).Value = Competition
                                            Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader()
                                            While RsRec.Read
                                                Competition = Convert.ToInt32(RsRec("Competition_Id"))
                                                Season = Convert.ToInt32(RsRec("Competition_Season"))
                                            End While
                                            RsRec.Close()

                                            Dim ds As New DataSet
                                            Dim da As New SqlDataAdapter
                                            SqlQuery = New SqlCommand("SELECT * FROM rugby.dbo.pa_TeamNames WHERE (Competition = @competition AND Season = @season);", DatabaseConn)
                                            SqlQuery.Parameters.Add("@competition", SqlDbType.Int).Value = Competition
                                            SqlQuery.Parameters.Add("@season", SqlDbType.Int).Value = Season
                                            da.SelectCommand = SqlQuery
                                            da.Fill(ds)

                                            If Season > 1 Then
                                                Dim xmlRound As XmlNode
                                                For Each xmlRound In xmlNode.SelectNodes("ROUND")
                                                    Dim RoundName As String = String.Empty
                                                    Dim xmlatr As XmlAttribute
                                                    For Each xmlatr In xmlRound.Attributes
                                                        Select Case xmlatr.Name
                                                            Case "NAME"
                                                                RoundName = xmlatr.InnerText
                                                        End Select
                                                    Next

                                                    'If RoundName.ToLower.IndexOf("pool") < 0 And RoundName.ToLower.IndexOf("group") < 0 And RoundName.ToLower.IndexOf("conference") < 0 And RoundName.ToLower.IndexOf("section") < 0 And RoundName.ToLower.IndexOf("premiership") < 0 And RoundName.ToLower.IndexOf("championship") < 0 Then
                                                    '    RoundName = String.Empty
                                                    'End If

                                                    If RoundName = "" And Competition = 345 Then
                                                        RoundName = "Combined Conferences Log"
                                                    End If

                                                    Dim RoundNumber As Integer = 1
                                                    If Not String.IsNullOrEmpty(RoundName) Then
                                                        SqlQuery = New SqlCommand("SELECT TOP 1 Round_Number FROM rugby.dbo.pa_Competitions WHERE (Competition_Id = @competition AND Competition_Season = @season AND Round_Name = @round)", DatabaseConn)
                                                        SqlQuery.Parameters.Add("@competition", SqlDbType.Int).Value = Competition
                                                        SqlQuery.Parameters.Add("@season", SqlDbType.Int).Value = Season
                                                        SqlQuery.Parameters.Add("@round", SqlDbType.VarChar).Value = RoundName
                                                        RsRec = SqlQuery.ExecuteReader()
                                                        While RsRec.Read
                                                            RoundNumber = Convert.ToInt32(RsRec("Round_Number"))
                                                        End While
                                                        RsRec.Close()
                                                    End If

                                                    sb.Append("DELETE FROM rugby.dbo.pa_Logs WHERE (Competition = " + Competition.ToString + " AND Season = " & Season.ToString + " AND Round = " & RoundNumber.ToString + ");")
                                                    Dim sorting As Integer = 1
                                                    Dim xmlRow As XmlNode
                                                    For Each xmlRow In xmlRound.SelectNodes("ROW")
                                                        Dim teamId As Integer = 0
                                                        Dim teamName As String = String.Empty
                                                        Dim played As Integer = 0
                                                        Dim won As Integer = 0
                                                        Dim lost As Integer = 0
                                                        Dim drew As Integer = 0
                                                        Dim pointsFor As Integer = 0
                                                        Dim pointsAgainst As Integer = 0
                                                        Dim pointsDifference As Integer = 0
                                                        Dim triesFor As Integer = 0
                                                        Dim triesAgainst As Integer = 0
                                                        Dim bonusPoints As Integer = 0
                                                        Dim points As Integer = 0
                                                        Dim rank As Integer = 1

                                                        If IsNumeric(xmlRow.Attributes("POSITION").InnerText) Then
                                                            rank = Convert.ToInt32(xmlRow.Attributes("POSITION").InnerText)
                                                        End If

                                                        Dim xmlDetail As XmlNode
                                                        For Each xmlDetail In xmlRow.ChildNodes
                                                            Select Case xmlDetail.Name
                                                                Case "TEAM"
                                                                    If IsNumeric(xmlDetail.Attributes("TEAMID").InnerText) Then
                                                                        teamId = Convert.ToInt32(xmlDetail.Attributes("TEAMID").InnerText)
                                                                    End If
                                                                    teamName = xmlDetail.InnerText
                                                                Case "PLAYED"
                                                                    If IsNumeric(xmlDetail.InnerText) Then
                                                                        played = Convert.ToInt32(xmlDetail.InnerText)
                                                                    End If
                                                                Case "WON"
                                                                    If IsNumeric(xmlDetail.InnerText) Then
                                                                        won = Convert.ToInt32(xmlDetail.InnerText)
                                                                    End If
                                                                Case "DRAWN"
                                                                    If IsNumeric(xmlDetail.InnerText) Then
                                                                        drew = Convert.ToInt32(xmlDetail.InnerText)
                                                                    End If
                                                                Case "LOST"
                                                                    If IsNumeric(xmlDetail.InnerText) Then
                                                                        lost = Convert.ToInt32(xmlDetail.InnerText)
                                                                    End If
                                                                Case "POINTSFOR"
                                                                    If IsNumeric(xmlDetail.InnerText) Then
                                                                        pointsFor = Convert.ToInt32(xmlDetail.InnerText)
                                                                    End If
                                                                Case "POINTSAGAINST"
                                                                    If IsNumeric(xmlDetail.InnerText) Then
                                                                        pointsAgainst = Convert.ToInt32(xmlDetail.InnerText)
                                                                    End If
                                                                Case "POINTSDIFFERENCE"
                                                                    If IsNumeric(xmlDetail.InnerText) Then
                                                                        pointsDifference = Convert.ToInt32(xmlDetail.InnerText)
                                                                    End If
                                                                Case "BONUSPOINTS"
                                                                    If IsNumeric(xmlDetail.InnerText) Then
                                                                        bonusPoints = Convert.ToInt32(xmlDetail.InnerText)
                                                                    End If
                                                                Case "POINTS"
                                                                    If IsNumeric(xmlDetail.InnerText) Then
                                                                        points = Convert.ToInt32(xmlDetail.InnerText)
                                                                    End If
                                                                Case "TRIESFOR"
                                                                    If IsNumeric(xmlDetail.InnerText) Then
                                                                        triesFor = Convert.ToInt32(xmlDetail.InnerText)
                                                                    End If
                                                                Case "TRIESAGAINST"
                                                                    If IsNumeric(xmlDetail.InnerText) Then
                                                                        triesAgainst = Convert.ToInt32(xmlDetail.InnerText)
                                                                    End If
                                                            End Select
                                                        Next

                                                        'Dim rows As DataRow() = ds.Tables(0).Select("teamId = '" + teamId.ToString + "'")
                                                        'If rows.Length > 0 Then
                                                        '    teamName = rows(0)("teamName").ToString
                                                        'End If

                                                        sb.Append("INSERT INTO rugby.dbo.pa_logs (competition, season, stage, round, rank, sorting, teamId, teamName, played, won, drawn, lost, pointsFor, pointsAgainst, pointsDifference, bonusPoints, points, created, triesFor, triesAgainst) VALUES (" + Competition.ToString + ", " + Season.ToString + ", 1, " & RoundNumber & ", " + rank.ToString + ", " + sorting.ToString + ", " + teamId.ToString + ", '" + teamName.Replace("'", "''") + "', " + played.ToString + ", " + won.ToString + ", " + drew.ToString + ", " + lost.ToString + ", " + pointsFor.ToString + ", " + pointsAgainst.ToString + ", " + pointsDifference.ToString + ", " + bonusPoints.ToString + ", " + points.ToString + ", GetDate(), " + triesFor.ToString + ", " + triesAgainst.ToString + ");")

                                                        sorting = sorting + 1
                                                    Next

                                                    SqlQuery = New SqlCommand(sb.ToString, DatabaseConn)
                                                    SqlQuery.ExecuteNonQuery()
                                                Next
                                            End If
                                    End Select
                                Next
                            End If
                        Catch ex As Exception
                            MoveFile = False
                            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                                DatabaseError = True
                            Else
                                Call ErrorHandler(MatchId, ex.Source & "-" & ex.Message, "Error with the TT log")
                            End If
                        Finally
                            xmlDoc = Nothing
                            sr.Close()
                            fileStream.Dispose()
                        End Try
                        If MoveFile = True Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log TT\Complete\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf DatabaseError = False Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log TT\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        End If
                    End If
                Next

                updateListbox("TT logs complete", True)
            End If
        End If

    End Sub

#End Region

#Region " Live "

    Private Sub Live()

        Dim File As String
        Dim Files() As String
        Dim Fi As FileInfo
        Dim MatchId As String = ""

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Live\Temporary") Then
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Live\Temporary")

            Dim Count As Integer = 0
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf(".xml") >= 0 Then
                    Count = Count + 1
                End If
            Next
            If Count > 0 Then
                updateListbox("Processing live", True)

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And DbConnected = True Then
                        Dim MoveFile As Integer = 1
                        Try
                            Dim xmlDoc As New XmlDocument
                            xmlDoc.Load(File)

                            If Fi.Name.IndexOf("LiveRugby") >= 0 Then
                                Call LiveMatch(xmlDoc)
                            ElseIf Fi.Name.IndexOf("RugbyCommentary") >= 0 Then
                                Call LiveCommentary(xmlDoc)
                            End If

                            xmlDoc = Nothing
                        Catch ex As Exception
                            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                                MoveFile = 3
                                'Call ErrorHandler(MatchId, ex.Message, "Error with live")
                            Else
                                MoveFile = 2
                                Call ErrorHandler(MatchId, ex.Message, "Error with live")
                            End If
                        Finally

                        End Try
                        If MoveFile = 1 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Live\Complete\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf MoveFile = 2 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Live\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        End If
                    End If
                Next

                updateListbox("Live complete", True)
            End If
        End If

    End Sub

    Private Function LiveMatch(ByVal xmlDoc As XmlDocument) As Integer

        Dim completed As Integer = 1
        Dim matchId As Integer = 1
        Dim homeTeamScore As Integer = 0
        Dim awayTeamScore As Integer = 0
        Dim status As String = ""
        Dim statusId As Integer = -1

        Try
            Dim xmlNode As XmlNode
            For Each xmlNode In xmlDoc.FirstChild.ChildNodes
                Select Case xmlNode.Name
                    Case "MATCHSUMMARY"
                        matchId = Convert.ToInt32(xmlNode.Attributes("MATCHID").InnerText)
                        If matchId = 2016167 Then
                            matchId = 3000001
                        End If
                        Dim xmlMatchNode As XmlNode
                        For Each xmlMatchNode In xmlNode.ChildNodes
                            Select Case xmlMatchNode.Name
                                Case "HOMETEAMSCORE"
                                    Dim xmlScoreNode As XmlNode = xmlMatchNode.SelectSingleNode("SCORE")
                                    If IsNumeric(xmlScoreNode.InnerText) Then
                                        homeTeamScore = Convert.ToInt32(xmlScoreNode.InnerText)
                                    End If
                                Case "AWAYTEAMSCORE"
                                    Dim xmlScoreNode As XmlNode = xmlMatchNode.SelectSingleNode("SCORE")
                                    If IsNumeric(xmlScoreNode.InnerText) Then
                                        awayTeamScore = Convert.ToInt32(xmlScoreNode.InnerText)
                                    End If
                                Case "MATCHSTATUS"
                                    status = xmlMatchNode.Attributes("STATUS").InnerText
                                    If status = "Abandoned" Then
                                        status = "Result"
                                    End If
                                    statusId = retLiveStatus(status)
                            End Select
                        Next

                        If statusId > -1 Then
                            Dim prevStatus As Integer = -1
                            SqlQuery = New SqlCommand("SELECT TOP 1 StatusId FROM rugby.dbo.pa_matches WHERE (Id = @matchId)", DatabaseConn)
                            SqlQuery.Parameters.Add("@matchId", SqlDbType.Int).Value = matchId
                            Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
                            While RsRec.Read
                                prevStatus = RsRec("StatusId")
                            End While
                            RsRec.Close()

                            If prevStatus > -1 Then
                                If statusId > prevStatus Then
                                    SqlQuery = New SqlCommand("UPDATE rugby.dbo.pa_matches SET statusId = @statusId, status = @status WHERE (Id = @matchId)", DatabaseConn)
                                    SqlQuery.Parameters.Add("@matchId", SqlDbType.Int).Value = matchId
                                    SqlQuery.Parameters.Add("@statusId", SqlDbType.Int).Value = statusId
                                    SqlQuery.Parameters.Add("@status", SqlDbType.VarChar).Value = status
                                    SqlQuery.ExecuteNonQuery()

                                    If statusId = 8 Or statusId = 14 Then
                                        SqlQuery = New SqlCommand("UPDATE rugby.dbo.pa_matches SET Result = 1 WHERE (Id = @matchId)", DatabaseConn)
                                        SqlQuery.Parameters.Add("@matchId", SqlDbType.Int).Value = matchId
                                        SqlQuery.ExecuteNonQuery()
                                    End If
                                End If

                                If homeTeamScore > 0 Or awayTeamScore > 0 Then
                                    SqlQuery = New SqlCommand("UPDATE rugby.dbo.pa_matches SET HomeTeamScore = @homeTeamScore, AwayTeamScore = @awayTeamScore WHERE (Id = @matchId)", DatabaseConn)
                                    SqlQuery.Parameters.Add("@matchId", SqlDbType.Int).Value = matchId
                                    SqlQuery.Parameters.Add("@homeTeamScore", SqlDbType.Int).Value = homeTeamScore
                                    SqlQuery.Parameters.Add("@awayTeamScore", SqlDbType.Int).Value = awayTeamScore
                                    SqlQuery.ExecuteNonQuery()
                                End If
                            End If
                        End If
                    Case "SQUADS"
                        Dim xmlSquadNode As XmlNode
                        For Each xmlSquadNode In xmlNode.ChildNodes
                            Dim xmlPlayers As XmlNodeList = xmlSquadNode.SelectNodes("PLAYER")
                            Dim xmlPlayer As XmlNode
                            Dim teamId As Integer = Convert.ToInt32(xmlSquadNode.Attributes("TEAMID").InnerText)
                            Dim squadQuery As New StringBuilder
                            squadQuery.Append("DELETE FROM rugby.dbo.pa_live_squads WHERE (MatchId = " + matchId.ToString() + " AND TeamId = " + teamId.ToString() + ");")
                            For Each xmlPlayer In xmlPlayers
                                Dim playerName As String = String.Empty
                                Dim playerId As String = String.Empty
                                Dim squadNo As Integer = 0
                                Dim subStitute As Integer = 0
                                Dim subStituteOn As String = -1
                                Dim subStituteOff As String = -1
                                Dim subStituteTime As Integer = -1

                                Dim xmlPlayerAttribute As XmlAttribute
                                For Each xmlPlayerAttribute In xmlPlayer.Attributes
                                    Select Case xmlPlayerAttribute.Name
                                        Case "PLNAME"
                                            playerName = xmlPlayerAttribute.InnerText
                                        Case "PLID"
                                            playerId = xmlPlayerAttribute.InnerText
                                        Case "SQUADNO"
                                            squadNo = Convert.ToInt32(xmlPlayerAttribute.InnerText)
                                        Case "SUB"
                                            If (xmlPlayerAttribute.InnerText = "Yes") Then
                                                subStitute = 1
                                            End If
                                        Case "SUBONID"
                                            subStituteOn = xmlPlayerAttribute.InnerText
                                        Case "SUBOFFID"
                                            subStituteOff = xmlPlayerAttribute.InnerText
                                        Case "SUBOFF"
                                            subStituteTime = Convert.ToInt32(xmlPlayerAttribute.InnerText)
                                        Case "SUBON"
                                            subStituteTime = Convert.ToInt32(xmlPlayerAttribute.InnerText)
                                    End Select
                                Next

                                squadQuery.Append("INSERT INTO rugby.dbo.pa_live_squads (matchId, teamId, playerId, playerName, squadNo, substitute, substitutionTime, substituteOn, substituteOff, created) VALUES (" + matchId.ToString() + ", " + teamId.ToString() + ", '" + playerId + "', '" + playerName.Replace("'", "''") + "', " + squadNo.ToString() + ", " + subStitute.ToString() + ", " + subStituteTime.ToString() + ", '" + subStituteOn + "', '" + subStituteOff + "', GetDate());")
                            Next

                            SqlQuery = New SqlCommand(squadQuery.ToString(), DatabaseConn)
                            SqlQuery.ExecuteNonQuery()
                        Next
                    Case "SCORERS"
                        Dim xmlScoreNode As XmlNode
                        Dim scorersQuery As New StringBuilder
                        scorersQuery.Append("DELETE FROM rugby.dbo.pa_live_events WHERE (MatchId = " + matchId.ToString() + " AND Event <> 10 AND Event <> 11);")
                        For Each xmlScoreNode In xmlNode.ChildNodes
                            Select Case xmlScoreNode.Name
                                Case "TRIES"
                                    Dim teamId As Integer = Convert.ToInt32(xmlScoreNode.Attributes("TEAMID").InnerText)
                                    Dim xmlScorers As XmlNodeList = xmlScoreNode.SelectNodes("SCORER")
                                    Dim xmlScorer As XmlNode
                                    For Each xmlScorer In xmlScorers
                                        Dim playerName As String = String.Empty
                                        Dim playerId As String = String.Empty
                                        Dim eventTime As Integer = -1

                                        Dim xmlPlayerAttribute As XmlAttribute
                                        For Each xmlPlayerAttribute In xmlScorer.Attributes
                                            Select Case xmlPlayerAttribute.Name
                                                Case "PLNAME"
                                                    playerName = xmlPlayerAttribute.InnerText
                                                Case "PLID"
                                                    playerId = xmlPlayerAttribute.InnerText
                                                Case "TIME"
                                                    eventTime = Convert.ToInt32(xmlPlayerAttribute.InnerText)
                                            End Select
                                        Next

                                        If playerName.ToLower() = "penalty try" Then
                                            scorersQuery.Append("INSERT INTO rugby.dbo.pa_live_events (matchId, teamId, event, playerId, playerName, time, created) VALUES (" + matchId.ToString() + ", " + teamId.ToString() + ", 6, 0, '" + "" + "', " + eventTime.ToString() + ", GetDate());")
                                        Else
                                            scorersQuery.Append("INSERT INTO rugby.dbo.pa_live_events (matchId, teamId, event, playerId, playerName, time, created) VALUES (" + matchId.ToString() + ", " + teamId.ToString() + ", 2, '" + playerId + "', '" + playerName.Replace("'", "''") + "', " + eventTime.ToString() + ", GetDate());")
                                        End If

                                    Next
                                Case "PENALTIES"
                                    Dim teamId As Integer = Convert.ToInt32(xmlScoreNode.Attributes("TEAMID").InnerText)
                                    Dim xmlScorers As XmlNodeList = xmlScoreNode.SelectNodes("SCORER")
                                    Dim xmlScorer As XmlNode
                                    For Each xmlScorer In xmlScorers
                                        Dim playerName As String = String.Empty
                                        Dim playerId As String = String.Empty
                                        Dim eventTime As Integer = -1

                                        Dim xmlPlayerAttribute As XmlAttribute
                                        For Each xmlPlayerAttribute In xmlScorer.Attributes
                                            Select Case xmlPlayerAttribute.Name
                                                Case "PLNAME"
                                                    playerName = xmlPlayerAttribute.InnerText
                                                Case "PLID"
                                                    playerId = xmlPlayerAttribute.InnerText
                                                Case "TIME"
                                                    eventTime = Convert.ToInt32(xmlPlayerAttribute.InnerText)
                                            End Select
                                        Next

                                        scorersQuery.Append("INSERT INTO rugby.dbo.pa_live_events (matchId, teamId, event, playerId, playerName, time, created) VALUES (" + matchId.ToString() + ", " + teamId.ToString() + ", 4, '" + playerId + "', '" + playerName.Replace("'", "''") + "', " + eventTime.ToString() + ", GetDate());")
                                    Next
                                Case "CONVERSIONS"
                                    Dim teamId As Integer = Convert.ToInt32(xmlScoreNode.Attributes("TEAMID").InnerText)
                                    Dim xmlScorers As XmlNodeList = xmlScoreNode.SelectNodes("SCORER")
                                    Dim xmlScorer As XmlNode
                                    For Each xmlScorer In xmlScorers
                                        Dim playerName As String = String.Empty
                                        Dim playerId As String = String.Empty
                                        Dim eventTime As Integer = -1

                                        Dim xmlPlayerAttribute As XmlAttribute
                                        For Each xmlPlayerAttribute In xmlScorer.Attributes
                                            Select Case xmlPlayerAttribute.Name
                                                Case "PLNAME"
                                                    playerName = xmlPlayerAttribute.InnerText
                                                Case "PLID"
                                                    playerId = xmlPlayerAttribute.InnerText
                                                Case "TIME"
                                                    eventTime = Convert.ToInt32(xmlPlayerAttribute.InnerText)
                                            End Select
                                        Next

                                        scorersQuery.Append("INSERT INTO rugby.dbo.pa_live_events (matchId, teamId, event, playerId, playerName, time, created) VALUES (" + matchId.ToString() + ", " + teamId.ToString() + ", 3, '" + playerId + "', '" + playerName.Replace("'", "''") + "', " + eventTime.ToString() + ", GetDate());")
                                    Next
                                Case "DROPGOALS"
                                    Dim teamId As Integer = Convert.ToInt32(xmlScoreNode.Attributes("TEAMID").InnerText)
                                    Dim xmlScorers As XmlNodeList = xmlScoreNode.SelectNodes("SCORER")
                                    Dim xmlScorer As XmlNode
                                    For Each xmlScorer In xmlScorers
                                        Dim playerName As String = String.Empty
                                        Dim playerId As String = String.Empty
                                        Dim eventTime As Integer = -1

                                        Dim xmlPlayerAttribute As XmlAttribute
                                        For Each xmlPlayerAttribute In xmlScorer.Attributes
                                            Select Case xmlPlayerAttribute.Name
                                                Case "PLNAME"
                                                    playerName = xmlPlayerAttribute.InnerText
                                                Case "PLID"
                                                    playerId = xmlPlayerAttribute.InnerText
                                                Case "TIME"
                                                    eventTime = Convert.ToInt32(xmlPlayerAttribute.InnerText)
                                            End Select
                                        Next

                                        scorersQuery.Append("INSERT INTO rugby.dbo.pa_live_events (matchId, teamId, event, playerId, playerName, time, created) VALUES (" + matchId.ToString() + ", " + teamId.ToString() + ", 5, '" + playerId + "', '" + playerName.Replace("'", "''") + "', " + eventTime.ToString() + ", GetDate());")
                                    Next
                            End Select
                        Next

                        SqlQuery = New SqlCommand(scorersQuery.ToString(), DatabaseConn)
                        SqlQuery.ExecuteNonQuery()
                    Case "DISCIPLINE"
                        Dim xmlDisciplineNode As XmlNode
                        Dim disciplineQuery As New StringBuilder
                        disciplineQuery.Append("DELETE FROM rugby.dbo.pa_live_events WHERE (MatchId = " + matchId.ToString() + " AND (Event = 10 OR Event = 11));")
                        For Each xmlDisciplineNode In xmlNode.ChildNodes
                            Select Case xmlDisciplineNode.Name
                                Case "CARDS"
                                    Dim teamId As Integer = Convert.ToInt32(xmlDisciplineNode.Attributes("TEAMID").InnerText)
                                    Dim xmlCards As XmlNodeList = xmlDisciplineNode.SelectNodes("CARD")
                                    Dim xmlCard As XmlNode
                                    For Each xmlCard In xmlCards
                                        Dim playerName As String = String.Empty
                                        Dim playerId As String = String.Empty
                                        Dim eventTime As Integer = -1
                                        Dim eventId As Integer

                                        Dim xmlPlayerAttribute As XmlAttribute
                                        For Each xmlPlayerAttribute In xmlCard.Attributes
                                            Select Case xmlPlayerAttribute.Name
                                                Case "PLNAME"
                                                    playerName = xmlPlayerAttribute.InnerText
                                                Case "PLID"
                                                    playerId = xmlPlayerAttribute.InnerText
                                                Case "TIME"
                                                    eventTime = Convert.ToInt32(xmlPlayerAttribute.InnerText)
                                                Case "TYPE"
                                                    If xmlPlayerAttribute.InnerText = "R" Then
                                                        eventId = 11
                                                    ElseIf xmlPlayerAttribute.InnerText = "Y" Then
                                                        eventId = 10
                                                    End If
                                            End Select
                                        Next

                                        disciplineQuery.Append("INSERT INTO rugby.dbo.pa_live_events (matchId, teamId, event, playerId, playerName, time, created) VALUES (" + matchId.ToString() + ", " + teamId.ToString() + ", " + eventId.ToString() + ", '" + playerId + "', '" + playerName.Replace("'", "''") + "', " + eventTime.ToString() + ", GetDate());")
                                    Next
                            End Select
                        Next

                        SqlQuery = New SqlCommand(disciplineQuery.ToString(), DatabaseConn)
                        SqlQuery.ExecuteNonQuery()
                End Select
            Next
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler(MatchId, ex.Message, "Error with live")
            End If
        End Try

        Return Completed

    End Function

    Private Sub LiveCommentary(ByVal xmlDoc As XmlDocument)

        Dim completed As Integer = 1
        Dim matchId As Integer = 1

        Try
            Dim xmlNode As XmlNode
            For Each xmlNode In xmlDoc.FirstChild.ChildNodes
                Select Case xmlNode.Name
                    Case "MATCH"
                        matchId = Convert.ToInt32(xmlNode.Attributes("MATCHID").InnerText)
                        If matchId = 2016167 Then
                            matchId = 3000001
                        End If
                    Case "COMMENTARY"
                        Dim xmlLines As XmlNodeList = xmlNode.SelectNodes("LINE")
                        Dim xmlLine As XmlNode
                        Dim commentaryQuery As New StringBuilder
                        commentaryQuery.Append("DELETE FROM rugby.dbo.pa_live_commentary WHERE (MatchId = " + matchId.ToString() + ");")
                        For Each xmlLine In xmlLines
                            Dim count As Integer = 0
                            Dim commentTime As Integer = -1
                            Dim commentText As String = String.Empty

                            If Not xmlLine.Attributes("COUNT") Is Nothing Then
                                If IsNumeric(xmlLine.Attributes("COUNT").InnerText) Then
                                    count = Convert.ToInt32(xmlLine.Attributes("COUNT").InnerText)
                                End If
                            End If

                            Dim xmlLineDetail As XmlNode
                            For Each xmlLineDetail In xmlLine.ChildNodes
                                Select Case xmlLineDetail.Name
                                    Case "MINUTES"
                                        If IsNumeric(xmlLineDetail.InnerText) Then
                                            commentTime = Convert.ToInt32(xmlLineDetail.InnerText)
                                        End If
                                    Case "TEXT"
                                        commentText = xmlLineDetail.InnerText
                                End Select
                            Next

                            commentaryQuery.Append("INSERT INTO rugby.dbo.pa_live_commentary (matchId, line, minute, text, created) VALUES (" + matchId.ToString() + ", " + count.ToString() + ", " + commentTime.ToString() + ", '" + commentText.Replace("'", "''") + "', GetDate());")
                        Next

                        SqlQuery = New SqlCommand(commentaryQuery.ToString(), DatabaseConn)
                        SqlQuery.ExecuteNonQuery()
                End Select
            Next
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                completed = 3
            Else
                completed = 2
                Call ErrorHandler(matchId, ex.Message, "Error with live")
            End If
        End Try

    End Sub

#End Region

#Region " News "

    Private Sub News()

        Dim File As String
        Dim Files() As String
        Dim Fi As FileInfo
        Dim Sites As ArrayList = New ArrayList
        Dim RsRec As SqlDataReader

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\News\Temporary") Then
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\News\Temporary")

            SqlQuery = New SqlCommand("Select Id From SuperSportZone.dbo.ZoneSites Where (Sport = 2) Order By Id", DatabaseConn)
            RsRec = SqlQuery.ExecuteReader
            While RsRec.Read
                Sites.Add(RsRec("Id"))
            End While
            RsRec.Close()

            Dim Count As Integer = 0
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf(".xml") >= 0 Then
                    Count = Count + 1
                End If
            Next
            If Count > 0 Then
                updateListbox("Processing news", True)

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And DbConnected = True Then
                        Dim MoveFile As Integer = 1
                        Try
                            Dim xmlDoc As New XmlDocument
                            xmlDoc.Load(File)
                            Dim MatchId As String = ""
                            Dim Headline As String = ""
                            Dim Blurb As String = ""
                            Dim Body As String = ""
                            Dim Competition As String = ""
                            Dim Category As Integer = 0
                            Dim CategoryName

                            Dim xmlMatches As XmlNodeList = xmlDoc.SelectNodes("SportReport/Match")
                            Dim O As Integer
                            For O = 0 To xmlMatches.Count - 1
                                Dim xmlMatch As XmlAttributeCollection = xmlMatches(O).Attributes
                                Dim I As Integer
                                For I = 0 To xmlMatch.Count - 1
                                    Dim tmpName As String = xmlMatch(I).Name
                                    Select Case tmpName
                                        Case "id"
                                            MatchId = xmlMatch(I).InnerText
                                        Case Else

                                    End Select
                                Next

                                SqlQuery = New SqlCommand("Select Top 1 a.HomeTeamName, a.AwayTeamName, b.articleCategory, c.Name As ArticleCategory From Rugby.dbo.pa_Matches a INNER JOIN Rugby.dbo.pa_Competitions b ON b.Competition_Id = a.Competition INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.articleCategory Where (a.Id = '" & MatchId & "' And a.Season = b.Competition_Season And a.Round = b.Round_Number And a.Stage = b.Stage_Number)", DatabaseConn)
                                RsRec = SqlQuery.ExecuteReader
                                While RsRec.Read
                                    Category = RsRec("articleCategory")
                                    CategoryName = RsRec("ArticleCategory")
                                    Headline = RsRec("HomeTeamName") & " v " & RsRec("AwayTeamName")
                                End While
                                RsRec.Close()

                                Body = xmlMatches(O).SelectSingleNode("Copy").InnerText
                                Dim tmpValue As String = Body.Substring(Body.IndexOf(".") + 1, 1)
                                Body = Body.Replace(tmpValue, "<br />")
                                If Body.StartsWith("<br />") Then
                                    Body = Body.Substring(6)
                                End If

                                Headline = Body.Substring(0, Body.IndexOf("<br />"))
                                Body = Body.Substring(Body.IndexOf("<br />") + 6)
                                If Body.StartsWith("<br />") Then
                                    Body = Body.Substring(6)
                                End If
                                Blurb = Body.Substring(0, Body.IndexOf("<br />"))

                                Body = Body.Replace("<br /><br />", "</p><p>")
                                Body = Body.Insert(0, "<p>")
                                Body += "</p>"
                                Body = Body.Replace("<br />", "")

                                If Blurb.Length > 240 Then
                                    Blurb = Blurb.Substring(0, 240)
                                    Blurb = Blurb.Substring(0, Blurb.LastIndexOf(" "))
                                    Blurb &= "..."
                                End If

                                If Body.Trim <> "" And Headline.Trim <> "" Then
                                    SqlQuery = New SqlCommand("Select Report From Rugby.dbo.pa_Matches Where (Id = '" & MatchId & "')", DatabaseConn)
                                    Dim Present As Integer = SqlQuery.ExecuteScalar

                                    If Present > 0 Then
                                        SqlQuery = New SqlCommand("Update SuperSportZone.dbo.ZoneArticles Set Headline = '" & Headline.Replace("'", "''").Trim & "', Blurb = '" & Blurb.Replace("'", "''").Trim & "', Body = '" & Body.Replace("'", "''").Trim & "', Modified = GetDate() Where (Id = " & Present & "); Update SuperSportZone.dbo.ArticleBreakDown Set Headline = '" & Headline.Replace("'", "''").Trim & "', Blurb = '" & Blurb.Replace("'", "''").Trim & "', Body = '" & Body.Replace("'", "''").Trim & "' Where (Id = " & Present & ");", DatabaseConn)
                                        SqlQuery.ExecuteNonQuery()

                                        UpdateHeadlines(Present)
                                    Else
                                        SqlQuery = New SqlCommand("Insert Into SuperSportZone.dbo.ZoneArticles (Headline, Blurb, Body, smallImage, smallImageAlt, largeImage, LargeImageAlt, Author, Credit, Created, Modified, Active, Utilised, Preview, Report, validFrom, validTo, image3, image3Alt, webOnly, Blog) Values ('" & Headline.Replace("'", "''").Trim & "', '" & Blurb.Replace("'", "''").Trim & "', '" & Body.Replace("'", "''").Trim & "', '', '', '', '', 109, 26, GetDate(), GetDate(), 1, 1, 0, 0, GetDate(), '2050-01-01 00:00:00', '', '', 0, 0); Select SCOPE_IDENTITY()", DatabaseConn)
                                        Dim retId As Integer = SqlQuery.ExecuteScalar

                                        SqlQuery = New SqlCommand("Insert Into SuperSportZone.dbo.ZoneArticleCategories (articleId, Category, articleLevel, articleDate, Created) Values (" & retId & ", " & Category & ", 0, GetDate(), GetDate())", DatabaseConn)
                                        SqlQuery.ExecuteNonQuery()

                                        SqlQuery = New SqlCommand("Update Rugby.dbo.pa_Matches Set Report = " & retId & " Where (Id = '" & MatchId & "')", DatabaseConn)
                                        SqlQuery.ExecuteNonQuery()

                                        UpdateHeadlines(retId)

                                        Dim QueryText As String = ""
                                        Dim IEnum As IEnumerator = Sites.GetEnumerator
                                        While IEnum.MoveNext
                                            If QueryText <> "" Then
                                                QueryText &= Environment.NewLine
                                            End If
                                            QueryText &= "Insert Into SuperSportZone.dbo.ArticleBreakDown (Id, CategoryId, CategoryName, Sport, Report, Preview, Author, Credit, ArticleDate, ArticleLevel, Active, Headline, Blurb, Body, SmallImage, LargeImage, SmallImageAlt, LargeImageAlt, Site, ValidFrom, ValidTo) Values (" & retId & ", " & Category & ", '" & CategoryName & "', 2, " & MatchId & ", 0, '', '', GetDate(), 0, 1, '" & Headline.Replace("'", "''").Trim & "', '" & Blurb.Replace("'", "''").Trim & "', '" & Body.Replace("'", "''").Trim & "', '', '', '', '', " & IEnum.Current & ", GetDate(), '2050-01-01 00:00:00')"
                                        End While
                                        If QueryText <> "" Then
                                            SqlQuery = New SqlCommand(QueryText, DatabaseConn)
                                            SqlQuery.ExecuteNonQuery()
                                        End If
                                    End If
                                End If
                            Next
                        Catch ex As Exception
                            MoveFile = 2
                            Call ErrorHandler("", ex.Message, "Error with the news")
                        End Try
                        If MoveFile = 1 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\News\Complete\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf MoveFile = 2 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\News\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        End If
                    End If
                Next

                updateListbox("News complete", True)
            End If
        End If

    End Sub

    Sub UpdateHeadlines(ByVal Id As Integer)

        Dim queryString As String = ""

        Try
            SqlQuery = New SqlCommand("Select a.Id, a.Headline, a.Created, b.Category, c.Site From SuperSportZone.dbo.ZoneArticles a INNER JOIN SuperSportZone.dbo.ZoneArticleCategories b ON b.ArticleId = a.Id INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category Where (a.Id = " & Id & ")", DatabaseConn)
            Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader()
            While RsRec.Read
                If queryString <> "" Then
                    queryString &= Environment.NewLine
                End If
                Dim Friendly As String = fixHeadline(RsRec("Headline").Trim())
                queryString &= "Insert Into SuperSportZone.dbo.ZoneArticleHeadlines (articleId, friendlyHeadline, created, modified, category, site) Values (" & Id & ", '" & Friendly & "', '" & RsRec("Created") & "', GetDate(), " & RsRec("Category") & ", " & RsRec("Site") & ");"
            End While
            RsRec.Close()

            If queryString <> "" Then
                SqlQuery = New SqlCommand(queryString, DatabaseConn)
                SqlQuery.ExecuteNonQuery()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Function fixHeadline(ByVal Headline As String)

        Dim retText As String = ""
        Headline = Headline.Trim()
        Dim Proceed As Boolean = True
        While Proceed = True
            Headline = Headline.Replace("  ", "")
            If Headline.IndexOf("  ") >= 0 Then
                Proceed = True
            Else
                Proceed = False
            End If
        End While

        Dim tmpArray() As String = Headline.Split(" ")
        Dim I As Integer
        For I = 0 To tmpArray.GetUpperBound(0)
            If retText <> "" Then
                retText &= "_"
            End If
            retText &= Regex.Replace(tmpArray(I), "\W", "")
        Next

        Proceed = True
        While Proceed = True
            retText = retText.Replace("__", "_")
            If retText.IndexOf("__") >= 0 Then
                Proceed = True
            Else
                Proceed = False
            End If
        End While

        Return retText

    End Function

#End Region

    Private Function retStatus(ByVal Status As String) As Integer

        Dim StatusId As Integer = 0

        Select Case Status
            Case "active"
                StatusId = 1
            Case "dormant"
                StatusId = 2
            Case "teamsin"
                StatusId = 3
            Case "kickoff"
                StatusId = 4
            Case "fulltimeextratime"
                StatusId = 5
            Case "halftime"
                StatusId = 6
            Case "secondhalfstarted"
                StatusId = 7
            Case "fulltime"
                StatusId = 8
            Case "fulltimeextratimetobeplayed"
                StatusId = 9
            Case "extratimestarted"
                StatusId = 10
            Case "extratimehalftime"
                StatusId = 11
            Case "extratimesecondhalfstarted"
                StatusId = 12
            Case "extratimefulltime"
                StatusId = 13
            Case "result"
                StatusId = 14
            Case "postponed"
                StatusId = 15
            Case "suspended"
                StatusId = 16
            Case "resumed"
                StatusId = 17
            Case "cancelled"
                StatusId = 18
            Case "abandoned"
                StatusId = 18
            Case Else
                StatusId = 0
        End Select

        Return StatusId

    End Function

    Private Function retLiveStatus(ByVal Status As String) As Integer

        Dim StatusId As Integer = -1

        Select Case Status
            Case "Not Started"
                StatusId = 0
            Case "First Half"
                StatusId = 4
            Case "Half Time"
                StatusId = 6
            Case "Second Half"
                StatusId = 7
            Case "Full Time"
                StatusId = 8
            Case "Extra Time"
                StatusId = 10
            Case "Result"
                StatusId = 14
            Case Else
                StatusId = 0
        End Select

        Return StatusId

    End Function

    Private Function retPAId(ByVal id As Integer) As Integer

        Dim returnId As Integer = id

        SqlQuery = New SqlCommand("SELECT TOP 1 Competition_Id FROM rugby.dbo.pa_competitions WHERE (leagueId = @id)", DatabaseConn)
        SqlQuery.Parameters.Add("@id", SqlDbType.Int).Value = id
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader()
        While RsRec.Read
            returnId = RsRec("Competition_Id")
        End While
        RsRec.Close()

        Return returnId

    End Function

#Region " Admin "

    Private Sub ErrorHandler(ByVal MatchId As String, ByVal LogError As String)

        Try
            Dim Fs As New FileStream("C:\Program Files\SuperSport Zone\SuperSport Rugby\log.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim Sw As New StreamWriter(Fs)
            Sw.BaseStream.Seek(0, SeekOrigin.End)
            Sw.WriteLine("" & DateTime.Now & " - " & MatchId & " - " & LogError & "")
            Sw.Close()
        Catch Ex As Exception

        End Try

    End Sub

    Private Sub ErrorHandler(ByVal MatchId As String, ByVal LogError As String, ByVal Message As String)

        Try
            Dim Fs As New FileStream("C:\Program Files\SuperSport Zone\SuperSport Rugby\log.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim Sw As New StreamWriter(Fs)
            Sw.BaseStream.Seek(0, SeekOrigin.End)
            Sw.WriteLine("" & DateTime.Now & " - " & MatchId & " - " & LogError & "")
            Sw.Close()

        Catch Ex As Exception

        End Try

        Try
            Dim tmpErrors As New Errors.Service
            If Proxy = True Then
                tmpErrors.Proxy = myProxy
            End If
            tmpErrors.Errors("Rugby", MatchId, LogError, 4, Message, True, Message)
        Catch Ex As Exception
            Dim Fs As New FileStream("C:\Program Files\SuperSport Zone\SuperSport Rugby\log.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim Sw As New StreamWriter(Fs)
            Sw.BaseStream.Seek(0, SeekOrigin.End)
            Sw.WriteLine(Ex.Message)
            Sw.Close()
        End Try

    End Sub

    Public Function CheckForExistingInstance() As Boolean

        Dim Multiple As Boolean = False

        If Process.GetProcessesByName(Process.GetCurrentProcess.ProcessName).Length > 1 Then
            Multiple = True
        End If

        Return Multiple

    End Function

    Public Function CheckForUpdate() As Boolean

        Dim Update As Boolean = False

        Try
            DatabaseConn.Open()
            SqlQuery = New SqlCommand("Select Version From SSZGeneral.dbo.ssprograms Where (Id = 4)", DatabaseConn)
            Dim tmpVersion As String = SqlQuery.ExecuteScalar
            If Version <> tmpVersion Then
                Shell("C:\Program Files\SuperSport Zone\SuperSport Updater\SuperSport Updater.exe rugby", AppWinStyle.NormalFocus)
                Me.WindowState = FormWindowState.Minimized
                Update = True
            Else
                Update = False
            End If
            DatabaseConn.Close()
        Catch Ex As Exception
            Update = False
        End Try

        Return Update

    End Function

    Private Function CheckForProxy() As Boolean

        If File.Exists("C:\Program Files\SuperSport Zone\SuperSport Active\proxyexists.txt") Then
            Dim Fs As New FileStream("C:\Program Files\SuperSport Zone\SuperSport Active\proxy.txt", FileMode.OpenOrCreate, FileAccess.Read)
            Dim Sw As New StreamReader(Fs)
            Sw.BaseStream.Seek(0, SeekOrigin.Begin)
            Dim proxyUsername As String = Sw.ReadLine
            Dim proxyPassword As String = Sw.ReadLine
            Dim proxyDomain As String = Sw.ReadLine
            Dim proxyAddress As String = Sw.ReadLine
            Dim proxyPort As Integer = Sw.ReadLine
            Dim ProxyCredentials As New System.Net.NetworkCredential(proxyUsername, proxyPassword, proxyDomain)
            myProxy = New System.Net.WebProxy(proxyAddress, proxyPort)
            myProxy.Credentials = ProxyCredentials
            Proxy = True
            Sw.Close()
        End If

    End Function

    Private Sub ZipFiles()

        If Not File.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\History\" & DateTime.Now.ToString("yyyyMMdd") & ".zip") Then
            Try
                updateListbox("Archiving files", True)
                Dim FixtureFolder As New DiskFolder("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture\Complete")
                Dim MatchFolder As New DiskFolder("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Match\Complete")
                Dim CompetitionFolder As New DiskFolder("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Competition\Complete")
                Dim LiveFolder As New DiskFolder("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Live\Complete")
                Dim LogFolder As New DiskFolder("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log\Complete")
                Dim NewsFolder As New DiskFolder("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\News\Complete")
                Dim tmpArchive As ZipArchive = New ZipArchive(New DiskFile("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\History\" & DateTime.Now.ToString("yyyyMMdd") & ".zip"))
                FixtureFolder.CopyFilesTo(tmpArchive, True, True)
                MatchFolder.CopyFilesTo(tmpArchive, True, True)
                CompetitionFolder.CopyFilesTo(tmpArchive, True, True)
                LiveFolder.CopyFilesTo(tmpArchive, True, True)
                LogFolder.CopyFilesTo(tmpArchive, True, True)
                NewsFolder.CopyFilesTo(tmpArchive, True, True)

                Dim File As String
                Dim Files() As String
                Dim Fi As FileInfo

                If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Match\Complete") Then
                    Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Match\Complete")
                    For Each File In Files
                        Fi = New FileInfo(File)
                        If Fi.Name.IndexOf(".xml") >= 0 Then
                            Try
                                Fi.Delete()
                            Catch ex As Exception

                            End Try
                        End If
                    Next
                End If

                If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture\Complete") Then
                    Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Fixture\Complete")
                    For Each File In Files
                        Fi = New FileInfo(File)
                        If Fi.Name.IndexOf(".xml") >= 0 Then
                            Try
                                Fi.Delete()
                            Catch ex As Exception

                            End Try
                        End If
                    Next
                End If

                If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Competition\Complete") Then
                    Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Competition\Complete")
                    For Each File In Files
                        Fi = New FileInfo(File)
                        If Fi.Name.IndexOf(".xml") >= 0 Then
                            Try
                                Fi.Delete()
                            Catch ex As Exception

                            End Try
                        End If
                    Next
                End If

                If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Live\Complete") Then
                    Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Live\Complete")
                    For Each File In Files
                        Fi = New FileInfo(File)
                        If Fi.Name.IndexOf(".xml") >= 0 Then
                            Try
                                Fi.Delete()
                            Catch ex As Exception

                            End Try
                        End If
                    Next
                End If

                If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log\Complete") Then
                    Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\Log\Complete")
                    For Each File In Files
                        Fi = New FileInfo(File)
                        If Fi.Name.IndexOf(".xml") >= 0 Then
                            Try
                                Fi.Delete()
                            Catch ex As Exception

                            End Try
                        End If
                    Next
                End If

                If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\News\Complete") Then
                    Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Rugby\Files\News\Complete")
                    For Each File In Files
                        Fi = New FileInfo(File)
                        If Fi.Name.IndexOf(".xml") >= 0 Then
                            Try
                                Fi.Delete()
                            Catch ex As Exception

                            End Try
                        End If
                    Next
                End If

                updateListbox("Files archived", True)
            Catch ex As Exception
                updateListbox("Error archiving files", True)
            End Try
        End If

    End Sub

#End Region

    Private Delegate Sub dlgUpdate(ByVal tmpString As String, ByVal tmpBoolean As Boolean)

    Sub updateListbox(ByVal tmpString As String, ByVal tmpBoolean As Boolean)

        If ListBox1.InvokeRequired = True Then
            Dim d As New dlgUpdate(AddressOf updateListbox)
            ListBox1.Invoke(d, tmpString, tmpBoolean)
        Else
            If tmpString = "" Then
                ListBox1.Items.Clear()
            ElseIf tmpString = "select" Then
                ListBox1.SelectedIndex = ListBox1.Items.Count - 1
            ElseIf tmpBoolean = True Then
                ListBox1.Items.Add(tmpString)
            Else
                ListBox1.Items.Remove(tmpString)
            End If
            ListBox1.Refresh()
        End If

    End Sub

    Sub updateButton(ByVal tmpString As String, ByVal tmpBoolean As Boolean)

        If Button1.InvokeRequired = True Then
            Dim d As New dlgUpdate(AddressOf updateButton)
            Button1.Invoke(d, tmpString, tmpBoolean)
        Else
            If tmpBoolean = True Then
                Button1.Enabled = True
                RunToolStripMenuItem.Enabled = True
            Else
                Button1.Enabled = False
                RunToolStripMenuItem.Enabled = False
            End If
        End If

    End Sub

End Class
